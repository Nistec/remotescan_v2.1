﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("RequestAsync start...");
            //RequestAsync();
            Console.WriteLine("RequestAsync finished...");
            Console.ReadLine();
        }

        static async void RequestAsync()
        {
            // ... Target page.
            string page = "https://localhost:4443/api/OpsApi/Scan";
            try
            {
                // ... Use HttpClient.
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage response = await client.GetAsync(page))
                using (HttpContent content = response.Content)
                {
                    // ... Read the string.
                    string result = await content.ReadAsStringAsync();

                    // ... Display the result.
                    if (result != null &&
                        result.Length >= 50)
                    {
                        Console.WriteLine("Response: " + result);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}

   

