#define SERVICE

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Threading;
using Nistec.Logging;
using System.Diagnostics;


namespace Controller
{
    public class Program
    {

#if (SERVICE)

        static void Main(string[] args)
        {

            try
            {

                if (args.Length > 0)
                {
                    //Install service
                    if (args[0].Trim().ToLower() == "/i")
                    { 
                        System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/i", Assembly.GetExecutingAssembly().Location }); 
                    }

                    //Uninstall service                 
                    else if (args[0].Trim().ToLower() == "/u")
                    { 
                        System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location }); 
                    }
                    //run as console application
                    else if (args[0].Trim().ToLower() == "/c")
                    {
                        AgentManager SVC = new AgentManager();
                        SVC.Start();
                        Console.WriteLine("Server Started as console");
                        Console.ReadKey();
                    }
                }
                else
                {

                    Console.WriteLine("Server Started...");

                    System.ServiceProcess.ServiceBase[] ServicesToRun;
                    ServicesToRun = new System.ServiceProcess.ServiceBase[] { new Service1() };
                    System.ServiceProcess.ServiceBase.Run(ServicesToRun);
                }
            }
            catch (Exception ex)
            {
                Netlog.Exception(Settings.ServiceName, ex, true, true);
            }
        }

#else

        static System.Threading.Mutex mutex = new System.Threading.Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        static void Main(string[] args)
        {
            //ApplicationLauncher.CreateProcessInConsoleSession("\"C:\\_Dev\\Scanner_Poc\\Debug\\RemoteScan.Proc\"", null, false);
 
            AgentManager SVC = null;// new ServiceManager();// (RunMode.Console);

            try
            {
                if (AlreadyRunning())
                {
                    Console.WriteLine("Instance Already Running!");
                    return;
                }
                SVC = new AgentManager();
                SVC.Start();
                Console.WriteLine("Server Started...");

                Thread.Sleep(1000);
                //Console.WriteLine("Server quit...");
                Console.ReadLine();


                //using (Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}"))
                //{
                //    if (!mutex.WaitOne(0, true))
                //    {
                //        Console.WriteLine("Instance Already Running!"); 
                //        return;
                //    }
                //    // Do stuff
                //}
                

                //if (mutex.WaitOne(TimeSpan.Zero, true))
                //{
                //    SVC = new ServiceManager();
                //    SVC.Start();
                //    Console.WriteLine("Server Started...");

                //    Thread.Sleep(1000);
                //    //Console.WriteLine("Server quit...");
                //    Console.ReadLine();
                //}
                //else
                //{
                //    Console.WriteLine("Instance Already Running!");
                //}

            }
            catch (Exception ex)
            {

                Console.Write("*****************ERROR**************");
                Console.Write(ex.Message);
                Console.Write(ex.StackTrace);
                Console.ReadLine();

            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        private static bool AlreadyRunning()
        {
            Process[] processes = Process.GetProcesses();
            Process currentProc = Process.GetCurrentProcess();

            foreach (Process process in processes)
            {
                try
                {
                    if (process.Modules[0].FileName == System.Reflection.Assembly.GetExecutingAssembly().Location
                                && currentProc.Id != process.Id)
                        return true;
                }
                catch (Exception)
                {

                }
            }

            return false;
        }

#endif


    }
}
