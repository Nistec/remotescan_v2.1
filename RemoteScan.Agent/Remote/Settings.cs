﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;

namespace Controller
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {

        public const string ServiceName = "RemoteScan.Agent";
        public const string DisplayName = "RemoteScan.Controller";
        public const string ServiceDescription = "RemoteScan Controller Agent";
        public static string[] ServicesDependedOn { get { return null /*new string[] { "Nistec.Controller", "Nistec.Queue" }*/; } }
        public static ServiceAccount ServiceAccount { get { return ServiceAccount.LocalSystem; } }
        public static ServiceStartMode ServiceStartMode { get { return ServiceStartMode.Automatic; } }


        public const string ServiceProcess = "RemoteScan.Agent.exe";
        public const string WindowsAppProcess = "ClalitRemoteScan.exe";
        public const string TrayAppProcess = "";
        public const string ManagerAppProcess = "";

        /// <summary>
        /// Gets if server service is installed.
        /// </summary>
        /// <returns></returns>
        public static bool IsServiceInstalled()
        {
            foreach (ServiceController service in ServiceController.GetServices())
            {
                if (service.ServiceName == Settings.ServiceName)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets server service installed.
        /// </summary>
        /// <param name="createIfNotInstalled"></param>
        /// <returns></returns>
        public static AgentController GetServiceInstalled()//bool createIfNotInstalled)
        {
            foreach (ServiceController service in ServiceController.GetServices())
            {
                if (service.ServiceName == Settings.ServiceName)
                {
                    return new AgentController(service);
                }
            }

            //if (createIfNotInstalled)
            //{
            //    //var service = new ServiceController();
            //    //service.DisplayName = Settings.DisplayName;
            //    //service.ServiceName = Settings.ServiceName;
            //    //return service;


            //}
            return new AgentController(null);
        }
    }

    public class AgentController
    {
        public AgentController(ServiceController sc)
        {
            ServiceController = sc;

            ServiceName = Settings.ServiceName;
            DisplayName = Settings.DisplayName;
            Installed = (sc != null);
        }

        public ServiceController ServiceController { get; set; }
        public string ServiceName { get; set; }
        public string DisplayName { get; set; }
        public bool Installed { get; set; }
    }

}
