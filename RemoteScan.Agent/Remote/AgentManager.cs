﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;
using Nistec.Logging;
using System.Web.Http.SelfHost;
using System.Web.Http;
using System.Configuration;
using Controller.Agent.Model;

namespace Controller
{

    public class AgentManager
    {
        HttpSelfHostServer apiServer;

        bool _loaded = false;
        bool EnableWebApiServer;
       
        public bool Loaded
        {
            get { return _loaded; }
        }

        public AgentManager()
        {
            EnableWebApiServer = true;
            //bool.TryParse(ConfigurationManager.AppSettings["EnableWebApiServer"], out EnableWebApiServer);
        }

        public void Start()
        {
            Thread Th = new Thread(new ThreadStart(InternalStart));
            Th.Start();
        }

        private void ApiStart()
        {
           
            //netsh http add urlacl url=http://+:80/MyUri user=DOMAIN\user
            string port = ConfigurationManager.AppSettings["http_Port"];
            string address = "http://localhost:" + port;

            LogListItems.Instance.AppendLog("Info: Scan api start : " + address, false);

            var config = new HttpSelfHostConfiguration(address);//"http://localhost:8082");
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            apiServer = new HttpSelfHostServer(config);
            apiServer.OpenAsync().Wait();

            LogListItems.Instance.AppendLog("Info: Scan api started!", false);

            Netlog.Debug("Ops api server started!");
        }

        private void ApiStop()
        {
            if (apiServer != null)
            {
                apiServer.CloseAsync();
                LogListItems.Instance.AppendLog("Info: Scan api stoped!", false);
                Netlog.Debug("Ops api server stoped!");
            }
        }
        private void InternalStart()
        {
            try
            {
                Netlog.Debug(Settings.ServiceName + " start...");
                if (EnableWebApiServer)
                {
                    ApiStart();
                }
                Netlog.Debug(Settings.ServiceName + " started!");
            }
            catch (Exception ex)
            {
                Netlog.Exception(Settings.ServiceName + " ", ex, true, true);
                LogListItems.Instance.AppendLog("Error: Scan api Exception " + ex.Message, false);
            }
        }

        public void Stop()
        {
            Netlog.Debug(Settings.ServiceName + " stop...");

            ApiStop();

            Netlog.Debug(Settings.ServiceName + " stoped.");
        }

    }

}
