//#define SERVICE

using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Configuration;
using Nistec.Channels;
using RemoteController.Model;
using Nistec.Logging;
using System.IO;
using RemoteController;

namespace Controller
{
	public class Service1 : System.ServiceProcess.ServiceBase
	{
        AgentManager Svc;
        ConfigFileWatcher cfWatcher;

        public Service1()
        {
            this.CanHandleSessionChangeEvent = true;
            Svc = new AgentManager();
            cfWatcher = new ConfigFileWatcher(ConfigFileWatcher.RemoteScanAgentFile);
            cfWatcher.FileChanged+=cfWatcher_FileChanged;
        }
        void cfWatcher_FileChanged(object sender, System.IO.FileSystemEventArgs e)
        {
            ScanSettings.Reset();
            if (Svc != null)
                Svc.Restart();
        }
		protected override void OnStart(string[] args)
		{
            Svc.Start();
            cfWatcher.Start(true);
		}
 
		protected override void OnStop()
		{
			Svc.Stop();
            cfWatcher.Stop();
		}
        string CurrentUser;
        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            base.OnSessionChange(changeDescription);
            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            try
            {
                switch (changeDescription.Reason)
                {
                    case SessionChangeReason.SessionLock:
                        break;
                    case SessionChangeReason.SessionUnlock:
                    case SessionChangeReason.ConsoleConnect:
                    case SessionChangeReason.RemoteConnect:
                        break;
                    case SessionChangeReason.SessionLogoff:
                        Netlog.Debug("OnSessionChange SessionLogoff, CurrentUser:  " + CurrentUser);
                        WinProc.KillProcess("RemoteScan.Proc");
                        break;
                    case SessionChangeReason.SessionLogon:
                        //Svc.SwitchChildProc();
                        Netlog.Debug("OnSessionChange SessionLogon, CurrentUser:  " + CurrentUser);

                        string location = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        string filename = Path.Combine(location, "RemoteScan.Proc.exe");
                        WinProc.RunAppAs("RemoteScan.Proc", filename, null, null, null);

                        //ApplicationLauncher.CreateProcessInConsoleSession("","", true);

                        Netlog.Debug("OnSessionChange SessionLogon Completed");
                        break;
                    case SessionChangeReason.RemoteDisconnect:
                        break;
                }
            }
            catch (Exception ex)
            {
                Netlog.Exception("OnSessionChange ", ex);
            }
            //changeDescription.SessionId

        }
    
        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);
            if (Svc == null)
                return;
            //Svc.DoCommand(command);
        }

        //protected override void OnShutdown()
        //{
        //    Svc.Stop();
        //}
      
	}
}
