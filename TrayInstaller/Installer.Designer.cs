﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Starter
{
    partial class Installer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public override void Commit(System.Collections.IDictionary savedState)
        {
            base.Commit(savedState);
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            if (Environment.Is64BitOperatingSystem)
            {
                Console.WriteLine("64bit");
            }

            Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ClalitRemoteScan.exe"));
            //Process.Start(Path.Combine(Path.GetDirectoryName(this.Context.Parameters["AssemblyPath"]), "ClalitRemoteScan.exe"));
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion
    }
}