﻿using Controller;
using RemoteController.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
  
namespace Starter
{
    class Program
    {
        const string MainApp = "NistecRemoteScan.exe";

        static void Main(string[] args)
        {

            Console.WriteLine("Starting NistecRemoteScan Starter");

            bool isservice = false;
            bool istray = false;
            bool isssl = false;
            bool launch = false;

            try
            {
                //args = new string[] { "a=1;b=1;c=0;d=1" };

                if (args == null || args.Length == 0)
                {
                    Console.WriteLine("Arguments required");
                    //SwitchUser();
                    return;
                }

                Console.WriteLine("arg: " + args[0]);

                string[] arguments = args[0].Split(';');

                if (arguments[0] == "u")
                {
                    isservice = true;
                    if (arguments.Length > 1)
                    {
                        isservice = arguments[1] == "c=1";
                    }
                    Uninstall(isservice);
                }
                else
                {
                    int i = 0;
                    foreach (string s in arguments)
                    {
                        Console.WriteLine("arg: " + s);
                        if (i == 0 && s == "a=1")
                            isssl = true;
                        if (i == 1 && s == "b=1")
                            launch = true;
                        if (i == 2 && s == "c=1")
                            isservice = true;
                        if (i == 3 && s == "d=1")
                            istray = true;
                        i++;
                    }
                    Install(isservice, istray, isssl, launch);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Starter error: " + ex.Message);
            }
            //Console.ReadLine();
        }

        static void SwitchUser()
        {
            Console.WriteLine("SwitchUser NistecRemoteScan Starter");
            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            location = @"C:\Program Files (x86)\Nistec\NistecRemoteScan\" + MainApp;// location.Replace("Starter.exe", MainApp);

            int traminated = WinProc.TerminateProcess("NistecRemoteScan");
            if (traminated > 0)
                WinProc.RunApp(location, null);
            Console.WriteLine("SwitchUser NistecRemoteScan finished");
        }
        static void Uninstall(bool isservice)
        {
            Console.WriteLine("Uninstall NistecRemoteScan Starter");

            SetStartup(-1);

            if (isservice)
            {
                var rsm = new RemoteServiceManager();
                if (rsm.IsServiceStarted())
                    rsm.DoServiceCommand(ServiceCmd.Stop);

                if (rsm.IsServiceInstalled())
                    rsm.DoServiceCommand(ServiceCmd.Uninstall);
            }
            //else
            //{
            //   WinProc.TerminateProcess("NistecRemoteScan");
            //}
            Console.WriteLine("Uninstall NistecRemoteScan finished");
        }
        static void Install(bool isservice, bool istray, bool isssl, bool launch)
        {
            Console.WriteLine("Install NistecRemoteScan Starter");

            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            const string pf64 = @"C:\Program Files (x86)";
            const string pf32 = @"C:\Program Files";

            const string nosslport = "key=\"http_Port\" value=\"8080\"";
            const string nosslcheck = "key=\"is_ssl\" value=\"false\"";

            const string sslport = "key=\"http_Port\" value=\"4443\"";
            const string sslcheck = "key=\"is_ssl\" value=\"true\"";


            //const string mode_tray = "key=\"running_mode\" value=\"tray\"";
            //const string mode_service = "key=\"running_mode\" value=\"service\"";
            //const string mode_both = "<add key=\"running_mode\" value=\"tray|service\"/>";

            List<string> values = new List<string>();
            List<string> replacment = new List<string>();

            if (isssl)
            {
                Console.WriteLine("Ssl mode");

                values.Add(nosslport);
                values.Add(nosslcheck);
                replacment.Add(sslport);
                replacment.Add(sslcheck);
            }

            if (Environment.Is64BitOperatingSystem)
            {
                Console.WriteLine("64bit");
            }
            else
            {
                Console.WriteLine("32bit");
                values.Add(pf64);
                replacment.Add(pf32);
            }

            //if (istray && isservice)
            //{
            //    values.Add(mode_tray);
            //    replacment.Add(mode_both);
            //}
            //else if (isservice)
            //{
            //    values.Add(mode_tray);
            //    replacment.Add(mode_service);
            //}
            //else //if (isservice)
            //{
            //    values.Add(mode_tray);
            //    replacment.Add(mode_tray);
            //}


            if (values.Count > 0)
            {
                ConfigEdit.FixConfig(path, values.ToArray(), replacment.ToArray());
            }

            if (isservice)
            {
                //string location = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                //string filename = Path.Combine(location, "RemoteScan.Proc.exe");
                //WinProc.RunAppAs("RemoteScan.Proc", filename, null,null,null);

                var rsm = new RemoteServiceManager();
                if (!rsm.IsServiceInstalled())
                    rsm.DoServiceCommand(ServiceCmd.Install);
                if (launch && !rsm.IsServiceStarted())
                    rsm.DoServiceCommand(ServiceCmd.Start);
            }

            if (istray)
            {
                SetStartup(1);
                if (launch)
                    Process.Start(Path.Combine(path, "NistecRemoteScan.exe"));
            }

            //Console.WriteLine("Running NistecRemoteScan");

            Console.WriteLine("Install NistecRemoteScan finished");
            //Console.ReadLine();

        }

        //mode: 1: startup , -1 :delete
        static void SetStartup(int mode)
        {
            try
            {
                //if (ScanSettings.enable_autostart)
                //{
                RegistryKey rk = Registry.LocalMachine.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
                location = location.Replace("Starter.exe", MainApp);
                string AppName = ScanSettings.AppName;

                if (mode == 1)
                    rk.SetValue(AppName, location);
                else if (mode == -1)
                    rk.DeleteValue(AppName, false);
                //}
                //ScanSettings.CreateLogFolder();
            }
            catch(Exception ex)
            {
                Console.WriteLine("SetStartup error: " + ex.Message);
            }
        }
    }
}
