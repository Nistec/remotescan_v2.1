﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Starter
{
    public class ConfigEdit
    {

        public static void FixConfig(string path, string[] values, string[] replacment)
        {
            if (path == null)
            {
                path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            if (values == null || replacment == null || values.Length == 0 || replacment.Length == 0 || values.Length != replacment.Length)
            {
                throw new ArgumentNullException("EditConfig.values");
            }

            Encoding encoding = Encoding.Default;
            string trayconfig = Path.Combine(path, "NistecRemoteScan.exe.config");
            string srvconfig = Path.Combine(path, "RemoteScan.Agent.exe.config");
            string procconfig = Path.Combine(path, "RemoteScan.Proc.exe.config");

            WriteConfig(trayconfig, values, replacment);
            WriteConfig(srvconfig, values, replacment);
            WriteConfig(procconfig, values, replacment);

            //using (var fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite))
            //{

            //    fs.Position = 0;
            //    using (StreamReader sr = new StreamReader(fs, encoding))
            //    {
            //        content = sr.ReadToEnd();

            //        int length = values.Length;

            //        for (int i = 0; i < length; i++)
            //        {
            //            content = content.Replace(values[i], replacment[i]);
            //        }

            //        Console.WriteLine("File raedy to write");
            //        fs.Position = 0;
            //        using (StreamWriter sw = new StreamWriter(fs, encoding))
            //        {
            //            sw.Write(content);
            //        }
            //    }

            //}

        }

        static void WriteConfig(string filename, string[] values, string[] replacment)
        {
            
            string content = "";


            content = File.ReadAllText(filename);

            int length = values.Length;

            for (int i = 0; i < length; i++)
            {
                content = content.Replace(values[i], replacment[i]);
            }

            File.WriteAllText(filename, content);

            //using (var fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite))
            //{

            //    fs.Position = 0;
            //    using (StreamReader sr = new StreamReader(fs, encoding))
            //    {
            //        content = sr.ReadToEnd();

            //        int length = values.Length;

            //        for (int i = 0; i < length; i++)
            //        {
            //            content = content.Replace(values[i], replacment[i]);
            //        }

            //        Console.WriteLine("File raedy to write");
            //        fs.Position = 0;
            //        using (StreamWriter sw = new StreamWriter(fs, encoding))
            //        {
            //            sw.Write(content);
            //        }
            //    }

            //}

        }
    }
}
