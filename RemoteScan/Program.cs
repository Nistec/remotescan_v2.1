﻿#pragma checksum "..\..\App.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "50E683F294D3A86CFFE162334643B17B"

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using RemoteScan.Controls;
using XamlGeneratedNamespace;
using System.Threading;
using RemoteController.Model;
using System.CodeDom.Compiler;

namespace RemoteScan
{

    /// <summary>
    /// GeneratedApplication
    /// </summary>
    public class ProgramApplication : System.Windows.Application
    {

        private bool _contentLoaded;
 
#if(true)
        #region InitializeComponent

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;

#line 3 "..\..\App.xaml"
            this.StartupUri = new System.Uri("MainNotifyWindow.xaml", System.UriKind.Relative);

#line default
#line hidden
            System.Uri resourceLocater = new System.Uri("/RemoteScan;component/app.xaml", System.UriKind.Relative);

#line 1 "..\..\App.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }
        #endregion



        static System.Threading.Mutex mutex = new System.Threading.Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");

        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public static void Main()
        {

            //string id= "RemoteScan-" +  Guid.NewGuid().ToString();
            //ScanSettings.TrayInstanceId = id;
            //WinProc.SetCurrentTitle(id);

            GeneratedApplication app = new GeneratedApplication();
            app.InitializeComponent();
            app.Run();

            //WinProc.TerminateProcessExceptCurrentSession("RemoteScan", id);


            //if (mutex.WaitOne(TimeSpan.Zero, true))
            //{
            //    ProgramApplication app = new ProgramApplication();
            //    app.InitializeComponent();
            //    app.Run();
            //    mutex.ReleaseMutex();
            //}
            //else
            //{
            //    // send our Win32 message to make the currently running instance
            //    // jump on top of all the other windows
            //    NativeMethods.PostMessage(
            //        (IntPtr)NativeMethods.HWND_BROADCAST,
            //        NativeMethods.WM_SHOWME,
            //        IntPtr.Zero,
            //        IntPtr.Zero);
            //}

        }

#else
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {

            if (this._contentLoaded)
            {
                return;
            }
            this._contentLoaded = true;
            base.Startup += new StartupEventHandler(this.Application_Startup);
            base.Exit += new ExitEventHandler(this.Application_Exit);
            this.StartupUri = new System.Uri("MainNotifyWindow.xaml", System.UriKind.Relative);
            Uri resourceLocator = new System.Uri("/RemoteScan;component/app.xaml", System.UriKind.Relative);
            Application.LoadComponent(this, resourceLocator);
        }

        private EventWaitHandle _singleInstanceEvent;
        private bool CheckSingleAppInstance()
        {
            bool result;
            this._singleInstanceEvent = new EventWaitHandle(false, EventResetMode.ManualReset, "RemoteScan: Single instance", out result);
            return result;
        }
        private void Stop()
        {
            try
            {
                base.Shutdown(0);
            }
            catch (Exception e)
            {
                LogListItems.Instance.Error("Failed to shutdown the application");
            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                if (!this.CheckSingleAppInstance())
                {
                    this.Stop();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                LogListItems.Instance.Error("Failed to start up the application, shutting down, " + ex.Message);
                this.Stop();
            }
        }

        private void Application_Exit(object sender, ExitEventArgs args)
        {
            LogListItems.Instance.Info("Application_Exit");
            //this.Uninit();
        }


        [GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode, STAThread]
        public static void Main()
        {
            ProgramApplication app = new ProgramApplication();
            app.InitializeComponent();
            app.Run();
        }

#endif

    }
}

