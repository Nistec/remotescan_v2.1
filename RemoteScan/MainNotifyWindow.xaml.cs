namespace RemoteScan
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media.Animation;
    using System.Windows.Media.Imaging;
    using ExtendedWindowsControls;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using RemoteController.Model;
    using System.Windows.Threading;
    using Microsoft.Win32;
    using Nistec.Channels;
    using Controller;
    using RemoteController;

    public partial class MainNotifyWindow : Window
    {
        private ExtendedNotifyIcon extendedNotifyIcon; // global class scope for the icon as it needs to exist foer the lifetime of the window
        private Storyboard gridFadeInStoryBoard;
        private Storyboard gridFadeOutStoryBoard;

        //AgentManager sm;
        //ConfigFileWatcher cfWatcher;
        //RemotePipeServer pipeserver;
        //IncomingFileWatcher incomingFileWatcher;
        //LogListItems logItem;

        NotifyController Nc;

        /// <summary>
        /// Sets up the popup window and instantiates the notify icon
        /// </summary>
        public MainNotifyWindow()
        {
            // Create a manager (ExtendedNotifyIcon) for handling interaction with the notification icon and wire up events. 
            extendedNotifyIcon = new ExtendedNotifyIcon();
            extendedNotifyIcon.MouseLeave += new ExtendedNotifyIcon.MouseLeaveHandler(extendedNotifyIcon_OnHideWindow);
            extendedNotifyIcon.MouseMove += new ExtendedNotifyIcon.MouseMoveHandler(extendedNotifyIcon_OnShowWindow);
            extendedNotifyIcon.targetNotifyIcon.Text = "Remote Scan";
            SetNotifyIcon("Blue");
            //ListItems=new List<string>();

            InitializeComponent();

            //this.Title = ScanSettings.TrayInstanceId;
            //WinProc.TerminateProcessExceptCurrentSession("RemoteScan", ScanSettings.TrayInstanceId);

            Nc = new NotifyController();

            //var logItem = LogListItems.Instance;
            Nc.logItem.LogChanged += Instance_LogChanged;

            // Set the startup position and the startup state to "not visible"
            SetWindowToBottomRightOfScreen();
            this.Opacity = 0;
            uiGridMain.Opacity = 0;
            //this.Focus();

            // Locate these storyboards and "cache" them - we only ever want to find these once for performance reasons
            gridFadeOutStoryBoard = (Storyboard)this.TryFindResource("gridFadeOutStoryBoard");
            gridFadeOutStoryBoard.Completed += new EventHandler(gridFadeOutStoryBoard_Completed);
            gridFadeInStoryBoard = (Storyboard)TryFindResource("gridFadeInStoryBoard");
            gridFadeInStoryBoard.Completed += new EventHandler(gridFadeInStoryBoard_Completed);

            //bool validateRemoteService = false;

            //if (ScanSettings.EnableService)
            //{
            //    if (MessageBox.Show("Do you wont to run remote scan service", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            //    {
            //        validateRemoteService = true;
            //    }
            //}

            Nc.Start();

            //CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //logItem.AppendLog("MainNotifyWindow start, CurrentUser:  " + CurrentUser);
        }

        void Instance_LogChanged(object sender, EventArgs e)
        {
            Dispatcher.Invoke(new Action(() => {
                this.listLog.Clear();
                this.listLog.AppendText(LogListItems.Instance.GetLog());

            }), DispatcherPriority.ContextIdle);
        }

        #region NotifyIcon events
        /// <summary>
        /// Pulls an icon from the packed resource and applies it to the NotifyIcon control
        /// </summary>
        /// <param name="iconPrefix"></param>
        private void SetNotifyIcon(string iconPrefix)
        {
            System.IO.Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,/Images/Scanner.ico")).Stream;

            //System.IO.Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,/Images/" + iconPrefix + "Orb.ico")).Stream;
            extendedNotifyIcon.targetNotifyIcon.Icon = new System.Drawing.Icon(iconStream);
        }

        /// <summary>
        /// Does what it says on the tin - ensures the popup window appears at the bottom right of the screen, just above the task bar
        /// </summary>
        private void SetWindowToBottomRightOfScreen()
        {
            Left = SystemParameters.WorkArea.Width - Width - 10;
            Top = SystemParameters.WorkArea.Height - Height;
        }

        /// <summary>
        /// When the notification manager requests the popup to be displayed through this event, perform the below actions
        /// </summary>
        void extendedNotifyIcon_OnShowWindow()
        {
            gridFadeOutStoryBoard.Stop();
            this.Opacity = 1; // Show the window (backing)
            this.Topmost = true; // Very rarely, the window seems to get "buried" behind others, this seems to resolve the problem
            if (uiGridMain.Opacity > 0 && uiGridMain.Opacity < 1) // If its animating, just set it directly to visible (avoids flicker and keeps the UX slick)
            {
                uiGridMain.Opacity = 1;
            }
            else if (uiGridMain.Opacity == 0)
            {
                gridFadeInStoryBoard.Begin();  // If it is in a fully hidden state, begin the animation to show the window
            }
        }

        /// <summary>
        /// When the notification manager requests the popup to be hidden through this event, perform the below actions
        /// </summary>
        void extendedNotifyIcon_OnHideWindow()
        {
            // if (PinButton.IsChecked == true) return; // Dont hide the window if its pinned open

            gridFadeInStoryBoard.Stop(); // Stop the fade in storyboard if running.

            // Only start fading out if fully faded in, otherwise you get a flicker effect in the UX because the animation resets the opacity
            if (uiGridMain.Opacity == 1 && this.Opacity == 1)
                gridFadeOutStoryBoard.Begin();
            else // Just hide the window and grid
            {
                uiGridMain.Opacity = 0;
                this.Opacity = 0;
            }
        }

        /// <summary>
        /// When the mouse enters the popup window's bounds, cancel any pending closing actions and immediately show the popup. 
        /// This is primarily to handle the case where the mouse termporarily leaves the popup window and returns again - 
        /// a UX / usability enhancement.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiWindowMainNotification_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            // Cancel the mouse leave event from firing, stop the fade out storyboard from running and enusre the grid is fully visible
            extendedNotifyIcon.StopMouseLeaveEventFromFiring();
            gridFadeOutStoryBoard.Stop();
            uiGridMain.Opacity = 1;
        }

        /// <summary>
        /// If the mouse leaves the popup, start the process to close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiWindowMainNotification_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (PinButton.IsChecked == false)
                extendedNotifyIcon_OnHideWindow();
        }

        /// <summary>
        /// Once the grid fades out, set the backing window to "not visible"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gridFadeOutStoryBoard_Completed(object sender, EventArgs e)
        {
            this.Opacity = 0;
        }

        /// <summary>
        /// Once the grid fades out, set the backing window to "visible"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gridFadeInStoryBoard_Completed(object sender, EventArgs e)
        {
            this.Opacity = 1;
        }

        /// <summary>
        /// When the pin button is pressed/unpressed, switch the icon appropriately. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PinButton_Click(object sender, RoutedEventArgs e)
        {
            if (PinButton.IsChecked == true)
                PinImage.Source = new BitmapImage(new Uri("pack://application:,,/Images/Pinned.png"));
            else
                PinImage.Source = new BitmapImage(new Uri("pack://application:,,/Images/Un-Pinned.png"));
        }

        /// <summary>
        /// Switch the icon depending on which colour is selected on the radio button group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void colourRadioButton_Click(object sender, RoutedEventArgs e)
        {
            SetNotifyIcon(((RadioButton)sender).Tag.ToString());
        }
        #endregion

        #region NotifyController evenents

        /// <summary>
        /// Shut down the popup window and dispose the notify icon (otherwise it hangs around in the task bar until you mouse over) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            //extendedNotifyIcon.Dispose();
            //this.Close();
            extendedNotifyIcon_OnHideWindow();
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            Nc.Start();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            bool stopService = false;
            try
            {
                if (ScanSettings.EnableService)
                {
                    if (MessageBox.Show("��� ����� �� ����� �������", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        stopService = true;
                    }
                }

                Nc.Stop(stopService);

                extendedNotifyIcon_OnHideWindow();
            }
            catch (Exception ex)
            {
                Nc.AppendLog("Error in tray Stop: " + ex.Message, true);
            }
            //extendedNotifyIcon_OnHideWindow();
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Nc.Quit();

                //SetStartup(-1);

                extendedNotifyIcon.Dispose();
                this.Close();

            }
            catch (Exception ex)
            {
                Nc.AppendLog("Error in tray Stop: " + ex.Message, true);
            }
            //extendedNotifyIcon.Dispose();
            //this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {

            try
            {
                base.OnClosed(e);

                Nc.Close();

                //SetStartup(-1);

                extendedNotifyIcon.Dispose();

            }
            catch (Exception ex)
            {
                Nc.AppendLog("Error in tray Stop: " + ex.Message, true);
            }
        }


        //private void CloseButton_Click(object sender, RoutedEventArgs e)
        //{
        //    extendedNotifyIcon.Dispose();
        //    this.Close();
        //}

        #endregion
    }

#if (false)
    public partial class MainNotifyWindow : Window
    {
        private ExtendedNotifyIcon extendedNotifyIcon; // global class scope for the icon as it needs to exist foer the lifetime of the window
        private Storyboard gridFadeInStoryBoard;
        private Storyboard gridFadeOutStoryBoard;

        AgentManager sm;
        ConfigFileWatcher cfWatcher;
        RemotePipeServer pipeserver;
        IncomingFileWatcher incomingFileWatcher;
        LogListItems logItem;

        //mode: 1: startup , -1 :delete
        void SetStartup(int mode)
        {
            if (ScanSettings.enable_autostart)
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string AppName = ScanSettings.AppName;

                if (mode == 1)
                    rk.SetValue(AppName, location);
                else if (mode == -1)
                    rk.DeleteValue(AppName, false);
            }
            ScanSettings.CreateLogFolder();
        }

        public void Start()
        {
            try
            {
                //SetStartup(1);

                if (ScanSettings.IsServiceMode)
                    return;

                //if (sm == null && ScanSettings.EnableTray)
                //{
                //    sm = new Controller.AgentManager();
                //    sm.Start();
                //    ScanSettings.CreateDocumentFolder();
                //}
                if (ScanSettings.EnableService)
                {

                    if (ScanSettings.EnableTray)
                    {
                        if (ScanSettings.tray_service_mode == "pipe")
                        {
                            if (pipeserver == null)
                            {
                                pipeserver = new RemotePipeServer();
                                pipeserver.Start();
                            }
                        }
                        else if (ScanSettings.tray_service_mode == "watcher")
                        {
                            if (incomingFileWatcher == null)
                            {
                                incomingFileWatcher = new IncomingFileWatcher(ScanSettings.document_path,"*.*");
                                incomingFileWatcher.FileChangedAction = (request) =>
                                {
                                    //string request = ScanController.ReadRequestFile(e.Name);
                                    //ScanController.DoScan(request);

                                    ScanController scan = new ScanController(request);
                                    scan.ExecScan();
                                };


                                //incomingFileWatcher.Init(ScanSettings.document_path, (string filename) =>
                                //{
                                //    string request=ScanController.ReadRequestFile(filename);
                                //    ScanController.DoScan(request);

                                //});
                                //incomingFileWatcher.Start();
                            }
                        }
                    }

                    var rsm = new RemoteServiceManager();
                    if (rsm.IsServiceInstalled())
                    {
                        if (!rsm.IsServiceStarted())
                            rsm.DoServiceCommand(ServiceCmd.Start);
                    }

                    //if (MessageBox.Show("��� ������ �� ����� �������", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    //{
                    //    var rsm = new RemoteServiceManager();
                    //    if (!rsm.IsServiceInstalled())
                    //        rsm.DoServiceCommand(ServiceCmd.Install);
                    //    if (!rsm.IsServiceStarted())
                    //        rsm.DoServiceCommand(ServiceCmd.Start);
                    //}
                }
                else
                {
                    if (sm == null && ScanSettings.EnableTray)
                    {
                        sm = new AgentManager();
                        sm.Start();
                        ScanSettings.CreateDocumentFolder();
                    }
                }

                if (cfWatcher == null)
                {
                    cfWatcher = new ConfigFileWatcher(ConfigFileWatcher.RemoteScanTrayFile);
                    cfWatcher.FileChanged += cfWatcher_FileChanged;
                    cfWatcher.Start(true);
                }
            }
            catch (Exception ex)
            {
                logItem.AppendLog("Error in tray Start: " + ex.Message, true);
            }
        }

        void cfWatcher_FileChanged(object sender, System.IO.FileSystemEventArgs e)
        {
            ScanSettings.Reset();
            if (sm != null)
                sm.Restart();
        }

        /// <summary>
        /// Sets up the popup window and instantiates the notify icon
        /// </summary>
        public MainNotifyWindow()
        {
            // Create a manager (ExtendedNotifyIcon) for handling interaction with the notification icon and wire up events. 
            extendedNotifyIcon = new ExtendedNotifyIcon();
            extendedNotifyIcon.MouseLeave += new ExtendedNotifyIcon.MouseLeaveHandler(extendedNotifyIcon_OnHideWindow);
            extendedNotifyIcon.MouseMove += new ExtendedNotifyIcon.MouseMoveHandler(extendedNotifyIcon_OnShowWindow);
            extendedNotifyIcon.targetNotifyIcon.Text = "Remote Scan";
            SetNotifyIcon("Blue");
            //ListItems=new List<string>();
                        
            InitializeComponent();

            logItem=LogListItems.Instance;
            logItem.LogChanged += Instance_LogChanged;

            // Set the startup position and the startup state to "not visible"
            SetWindowToBottomRightOfScreen();
            this.Opacity = 0;
            uiGridMain.Opacity = 0;
            //this.Focus();

            // Locate these storyboards and "cache" them - we only ever want to find these once for performance reasons
            gridFadeOutStoryBoard = (Storyboard)this.TryFindResource("gridFadeOutStoryBoard");
            gridFadeOutStoryBoard.Completed += new EventHandler(gridFadeOutStoryBoard_Completed);
            gridFadeInStoryBoard = (Storyboard)TryFindResource("gridFadeInStoryBoard");
            gridFadeInStoryBoard.Completed += new EventHandler(gridFadeInStoryBoard_Completed);


            SystemEvents.SessionEnding += SystemEvents_SessionEnding;
            SystemEvents.SessionEnded += SystemEvents_SessionEnded;
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;


            Start();

            //CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //logItem.AppendLog("MainNotifyWindow start, CurrentUser:  " + CurrentUser);
        }

        

        public void StartAgent()
        {
            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            logItem.AppendLog("OnSessionChange SessionLogon, CurrentUser:  " + CurrentUser);

            if (sm == null && ScanSettings.EnableTray)
            {
                sm = new AgentManager();
                sm.Start();
            }
        }
        public void StopAgent()
        {
            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            logItem.AppendLog("OnSessionEnded SessionLogoff, CurrentUser:  " + CurrentUser);
            if (sm != null)
            {
                sm.Stop();
                sm = null;
            }
        }

        void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            StopAgent();
        }
        void SystemEvents_SessionEnded(object sender, SessionEndedEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionEndReasons.Logoff:
                case SessionEndReasons.SystemShutdown:
                default:
                    StopAgent();
                    break;
            }
        }

        string CurrentUser;
        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            //logItem.AppendLog("OnSessionChange SessionLogon Start...");


            try
            {
                switch (e.Reason)
                {
                    case SessionSwitchReason.SessionLock:
                        break;
                    case SessionSwitchReason.SessionUnlock:
                    case SessionSwitchReason.ConsoleConnect:
                    case SessionSwitchReason.RemoteConnect:
                        break;
                    case SessionSwitchReason.SessionLogoff:
                        //logItem.AppendLog("OnSessionChange SessionLogoff, CurrentUser:  " + CurrentUser);
                        //WinProc.KillProcess("RemoteScan.Proc");

                        StopAgent();

                        break;
                    case SessionSwitchReason.SessionLogon:
                        //Svc.SwitchChildProc();
                        
                        StartAgent();

                        //string location = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        //string filename = Path.Combine(location, "RemoteScan.Proc.exe");
                        //WinProc.RunAppAs("RemoteScan.Proc", filename, null, null, null);

                        //WinProc.TerminateProcessExceptCurrentSession("RemoteScan.Proc");

                        //ApplicationLauncher.CreateProcessInConsoleSession("","", true);

                        //logItem.AppendLog("OnSessionChange SessionLogon Completed");
                        break;
                    case SessionSwitchReason.RemoteDisconnect:
                        break;
                }
            }
            catch (Exception ex)
            {
                logItem.AppendLog("OnSessionChange Error: " + ex.Message, true);
            }
            //changeDescription.SessionId
        }

        void Instance_LogChanged(object sender, EventArgs e)
        {
            Dispatcher.Invoke(new Action(() => {
                this.listLog.Clear();
                this.listLog.AppendText(LogListItems.Instance.GetLog());
            
            }), DispatcherPriority.ContextIdle);
        }

    #region NotifyIcon events
        /// <summary>
        /// Pulls an icon from the packed resource and applies it to the NotifyIcon control
        /// </summary>
        /// <param name="iconPrefix"></param>
        private void SetNotifyIcon(string iconPrefix)
        {
            System.IO.Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,/Images/Scanner.ico")).Stream;

            //System.IO.Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,/Images/" + iconPrefix + "Orb.ico")).Stream;
            extendedNotifyIcon.targetNotifyIcon.Icon = new System.Drawing.Icon(iconStream);
        }

        /// <summary>
        /// Does what it says on the tin - ensures the popup window appears at the bottom right of the screen, just above the task bar
        /// </summary>
        private void SetWindowToBottomRightOfScreen()
        {
            Left = SystemParameters.WorkArea.Width - Width - 10;
            Top = SystemParameters.WorkArea.Height - Height;
        }

        /// <summary>
        /// When the notification manager requests the popup to be displayed through this event, perform the below actions
        /// </summary>
        void extendedNotifyIcon_OnShowWindow()
        {
            gridFadeOutStoryBoard.Stop();
            this.Opacity = 1; // Show the window (backing)
            this.Topmost = true; // Very rarely, the window seems to get "buried" behind others, this seems to resolve the problem
            if (uiGridMain.Opacity > 0 && uiGridMain.Opacity < 1) // If its animating, just set it directly to visible (avoids flicker and keeps the UX slick)
            {
                uiGridMain.Opacity = 1;
            }
            else if (uiGridMain.Opacity == 0)
            {
                gridFadeInStoryBoard.Begin();  // If it is in a fully hidden state, begin the animation to show the window
            }
        }

        /// <summary>
        /// When the notification manager requests the popup to be hidden through this event, perform the below actions
        /// </summary>
        void extendedNotifyIcon_OnHideWindow()
        {
           // if (PinButton.IsChecked == true) return; // Dont hide the window if its pinned open

            gridFadeInStoryBoard.Stop(); // Stop the fade in storyboard if running.

            // Only start fading out if fully faded in, otherwise you get a flicker effect in the UX because the animation resets the opacity
            if (uiGridMain.Opacity == 1 && this.Opacity == 1)
                gridFadeOutStoryBoard.Begin();
            else // Just hide the window and grid
            {
                uiGridMain.Opacity = 0;
                this.Opacity = 0;
            }
        }

        /// <summary>
        /// When the mouse enters the popup window's bounds, cancel any pending closing actions and immediately show the popup. 
        /// This is primarily to handle the case where the mouse termporarily leaves the popup window and returns again - 
        /// a UX / usability enhancement.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiWindowMainNotification_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            // Cancel the mouse leave event from firing, stop the fade out storyboard from running and enusre the grid is fully visible
            extendedNotifyIcon.StopMouseLeaveEventFromFiring(); 
            gridFadeOutStoryBoard.Stop(); 
            uiGridMain.Opacity = 1;
        }

        /// <summary>
        /// If the mouse leaves the popup, start the process to close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiWindowMainNotification_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (PinButton.IsChecked == false)
                extendedNotifyIcon_OnHideWindow();
        }
 
        /// <summary>
        /// Once the grid fades out, set the backing window to "not visible"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gridFadeOutStoryBoard_Completed(object sender, EventArgs e)
        {
            this.Opacity = 0;
        }

        /// <summary>
        /// Once the grid fades out, set the backing window to "visible"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void gridFadeInStoryBoard_Completed(object sender, EventArgs e)
        {
            this.Opacity = 1;
        }

        /// <summary>
        /// When the pin button is pressed/unpressed, switch the icon appropriately. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PinButton_Click(object sender, RoutedEventArgs e)
        {
            if (PinButton.IsChecked == true)
                PinImage.Source = new BitmapImage(new Uri("pack://application:,,/Images/Pinned.png"));
            else
                PinImage.Source = new BitmapImage(new Uri("pack://application:,,/Images/Un-Pinned.png"));
        }

        /// <summary>
        /// Switch the icon depending on which colour is selected on the radio button group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void colourRadioButton_Click(object sender, RoutedEventArgs e)
        {
            SetNotifyIcon(((RadioButton)sender).Tag.ToString());
        }
    #endregion

        /// <summary>
        /// Shut down the popup window and dispose the notify icon (otherwise it hangs around in the task bar until you mouse over) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            //extendedNotifyIcon.Dispose();
            //this.Close();
            extendedNotifyIcon_OnHideWindow();
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            Start();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sm != null)
                {
                    sm.Stop();
                    sm = null;
                }
                if (ScanSettings.EnableService)
                {
                    if (MessageBox.Show("��� ����� �� ����� �������", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        var rsm = new RemoteServiceManager();
                        rsm.DoServiceCommand(ServiceCmd.Stop);
                    }
                }
            }
            catch (Exception ex)
            {
                logItem.AppendLog("Error in tray Stop: " + ex.Message, true);
            }
            extendedNotifyIcon_OnHideWindow();
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sm != null)
                {
                    sm.Stop();
                    sm = null;
                }
                if (ScanSettings.EnableService)
                {
                    if (pipeserver != null)
                    {
                        pipeserver.Stop();
                        pipeserver = null;
                    }
                    //if (MessageBox.Show("��� ����� �� ����� �������", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    //{
                    //    var rsm = new RemoteServiceManager();
                    //    rsm.DoServiceCommand(ServiceCmd.Stop);
                    //}
                }
                if (cfWatcher == null)
                {
                    cfWatcher.Stop();
                    cfWatcher = null;
                }

                //SetStartup(-1);
            }
            catch (Exception ex)
            {
                logItem.AppendLog("Error in tray Stop: " + ex.Message, true);
            }
            extendedNotifyIcon.Dispose();
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            try
            {
                if (sm != null)
                {
                    sm.Stop();
                    sm = null;
                }
                if (ScanSettings.EnableService)
                {
                    if (pipeserver != null)
                    {
                        pipeserver.Stop();
                        pipeserver = null;
                    }
                    //if (MessageBox.Show("��� ����� �� ����� �������", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    //{
                    //    var rsm = new RemoteServiceManager();
                    //    rsm.DoServiceCommand(ServiceCmd.Stop);
                    //}
                }
                if (cfWatcher == null)
                {
                    cfWatcher.Stop();
                    cfWatcher = null;
                }

                //SetStartup(-1);

                extendedNotifyIcon.Dispose();

            }
            catch (Exception ex)
            {
                logItem.AppendLog("Error in tray Stop: " + ex.Message, true);
            }
        }

              
        //private void CloseButton_Click(object sender, RoutedEventArgs e)
        //{
        //    extendedNotifyIcon.Dispose();
        //    this.Close();
        //}
    }
#endif
}