﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;

namespace ServiceCommander
{
    class Program
    {


        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to Service commander....");

            ServiceController sc = null;

            
            string srvname = null;
            string subcmd = null;
            string cmd = "run";
            int icommand = 0;
            do
            {
                Console.WriteLine("Command...");
                cmd = Console.ReadLine();
                try
                {
                    if (cmd.Length > 0)
                    {
                        string[] c = cmd.Split(' ');
                        subcmd = cmd.Replace(c[0], "").Trim();
                        cmd = c[0].Trim();
                    }

                    switch (cmd)
                    {
                        case "open":
                            srvname = subcmd;
                            if (sc != null && sc.ServiceName == srvname)
                            {
                                Console.WriteLine("ServiceController allready connected to :{0}", srvname);
                                goto exit_label;
                            }
                            sc = new ServiceController(srvname);
                            if (sc.ServiceHandle.IsInvalid)
                            {
                                Console.WriteLine("ServiceController is invalid :{0}", srvname);
                                sc = null;
                                goto exit_label;

                            }
                            Console.WriteLine("ServiceController connected to :{0}", srvname);
                            break;
                        case "exec":
                            icommand = 0;
                            int.TryParse(subcmd, out icommand);
                            if (icommand > 255 || icommand < 128)
                            {
                                icommand = 0;
                                Console.WriteLine("ServiceController incorrecrt command :{0}", subcmd);
                                goto exit_label;
                            }
                            if (sc== null || sc.ServiceHandle.IsInvalid)
                            {
                                Console.WriteLine("ServiceController is invalid :{0}", srvname);
                                sc = null;
                                goto exit_label;
                            }
                            sc.ExecuteCommand(icommand);
                            Console.WriteLine("ExecuteCommand completed :{0}", icommand);
                            break;
                        case "dispose":
                            if (sc != null)
                            {
                                sc.Dispose();
                                sc = null;
                                Console.WriteLine("ServiceController disposed :{0}", srvname);
                            }
                            break;
                        case "quit":

                            break;
                        case "?":
                        case "help":
                            PrintHelp();
                            break;
                        default:
                            Console.WriteLine("Unknwon command!");
                            break;
                    }
                exit_label:
                    Console.Write("");
                }
                catch (InvalidOperationException oex)
                {
                    sc = null;
                    srvname = null;
                    Console.WriteLine("Error :{0}", oex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error :{0}", ex.Message);
                }
            }
            while (cmd != "quit");
        }

        static string ReadCommand(string display)
        {
            Console.WriteLine(display);
            return Console.ReadLine();
        }

        static void PrintHelp()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Service commander commands are:");
            sb.AppendLine("/open [service name]: connect to service controller");
            sb.AppendLine("/exec [command]: execute command between 128 to 255");
            sb.AppendLine("/dispose: dispose the current service controller");
            sb.AppendLine("/quit: quit commander");
            sb.AppendLine("/help: print commander help");
            Console.WriteLine(sb.ToString());
         }

    }
}
