﻿using NAPS2.Scan;
using Nistec.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDeviceConfiger.Device
{
    class ProfileSettings
    {

        public static string[] GetExcludeList()
        {
            var str = NetConfig.AppSettings["ExcludeDevices"];
            return str == null ? null : str.SplitTrim(";");
        }

        public static string AutoSavingPath
        {
            get { return NetConfig.AppSettings["AutoSavingPath"]; }
        }
        public static ScanBitDepth BitDepth
        {
            get { return EnumExtension.Parse <ScanBitDepth>(NetConfig.AppSettings["BitDepth"], ScanBitDepth.Grayscale); }
        }
        public static ScanSource ScanSource
        {
            get { return EnumExtension.Parse<ScanSource>(NetConfig.AppSettings["ScanSource"], ScanSource.Feeder); }
        }
        public static ScanDpi ScanDpi
        {
            get { return EnumExtension.Parse<ScanDpi>(NetConfig.AppSettings["ScanDpi"], ScanDpi.Dpi200); }
        }

        
        
        
    }
}
