﻿using NAPS2.Scan;
using Nistec.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDeviceConfiger.Device
{
    class AgentManager
    {

        string[] GetExcludeList()
        {
            var str = NetConfig.AppSettings["ExcludeDevices"];
            return str == null ? null : str.SplitTrim(";");
        }

        ScanDevice PreaperTwainDevice()
        {
            var el= GetExcludeList();
            TwainDeviceWrapper twr = new TwainDeviceWrapper();
            var dl = twr.GetTwainDeviceList();
            foreach (var d in dl)
            {
                Console.WriteLine(d.ID + ", " + d.Name);

                if (el.Contains(d.Name))
                    continue;
                return d;
            }
            return null;
        }

        ScanProfile LoadProfile()
        {
            ProfileManager pm = new ProfileManager();
            pm.Load();
            var profile = pm.DefaultProfile;
            return profile;
        }

        ScanProfile EditProfile(ProfileManager pm, ScanDevice device)
        {
            pm.Load();
            var profile = pm.DefaultProfile;

            //ScanProfile profile = LoadProfile();
            if (profile==null)
            {
                profile = new ScanProfile();
            }
            profile.Device = device;
            profile.EnableAutoSave = true;
            profile.AutoSaveSettings = new AutoSaveSettings()
            {
                ClearImagesAfterSaving = false,
                FilePath = ProfileSettings.AutoSavingPath,
                Separator = NAPS2.ImportExport.SaveSeparator.FilePerScan
            };
            profile.BitDepth = ProfileSettings.BitDepth;
            profile.DisplayName = device.Name;
            profile.DriverName = device.DriverName;
            profile.PageSize = ScanPageSize.A4;
            profile.PaperSource = ProfileSettings.ScanSource;
            profile.Resolution = ProfileSettings.ScanDpi;
            profile.UseNativeUI = false;
            return profile;
        }

        public void StoreProfile()
        {

            var device = PreaperTwainDevice();
            if (device == null)
            {
                throw new Exception("Device not found!");
            }
            ProfileManager pm = new ProfileManager();//@"C:\_Dev\Scanner_Poc\ScanDeviceConsole\ScanDeviceConsole\bin\Debug");
            var profile = EditProfile(pm, device);
            if (pm.Profiles.Count==0)
            {
                pm.Profiles.Add(profile);
            }
            
            pm.Save();

        }
    }
}
