﻿using NAPS2.Host;
using NAPS2.Scan;
using NAPS2.Scan.Twain;
using NAPS2.WinForms;
using NTwain;
using NTwain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ScanDeviceConfiger.Device
{
    public class TwainDeviceWrapper
    {


        public List<ScanDevice> GetTwainDeviceList()
        {
            TwainImpl twainImpl = TwainImpl.Default;// ScanProfile != null ? ScanProfile.TwainImpl : TwainImpl.Default;
            TWIdentity TwainAppId = TWIdentity.CreateFromAssembly(DataGroups.Image | DataGroups.Control, Assembly.GetEntryAssembly());

            PlatformInfo.Current.PreferNewDSM = twainImpl != TwainImpl.OldDsm;
            var session = new TwainSession(TwainAppId);
            session.Open();
            try
            {
                return session.GetSources().Select(ds => new ScanDevice(ds.Name, ds.Name)).ToList();
            }
            finally
            {
                session.Close();
            }
        }

    }



    /*
    class ScanDevices : TwainScanDriver
    {

        //public const string DRIVER_NAME = "twain";

        //private readonly IX86HostServiceFactory x86HostServiceFactory;
        //private readonly TwainWrapper twainWrapper;
        //private readonly IFormFactory formFactory;

        //public static ScanDevices Create()
        //{
        //    IX86HostServiceFactory x86HostServiceFactory=new ;
        //}


        //public ScanDevices():base()
        //{

        //}

        public void FindScanner()
        {
            List<ScanDevice> devices = GetScanDeviceList();
        }


        public List<ScanDevice> GetScanDeviceList()
        {
            return GetDeviceListInternal();
        }

        //private bool UseHostService
        //{
        //    get { return ScanProfile.TwainImpl != TwainImpl.X64 && Environment.Is64BitProcess; }
        //}

        //protected override List<ScanDevice> GetDeviceListInternal()
        //{
        //    // Exclude WIA proxy devices since NAPS2 already supports WIA
        //    return GetFullDeviceList().Where(x => !x.ID.StartsWith("WIA-")).ToList();
        //}

        //private IEnumerable<ScanDevice> GetFullDeviceList()
        //{
        //    var twainImpl = ScanProfile != null ? ScanProfile.TwainImpl : TwainImpl.Default;
        //    if (UseHostService)
        //    {
        //        return x86HostServiceFactory.Create().TwainGetDeviceList(twainImpl);
        //    }
        //    return twainWrapper.GetDeviceList(twainImpl);
        //}


    }

    */
}
