﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.Filters;
//using System.Web.Mvc;

namespace RemoteController
{

    public class AllowCrossSiteAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response != null)
                actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");

            base.OnActionExecuted(actionExecutedContext);
        }


        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        //{

        //    var ctx = filterContext.RequestContext.HttpContext;
        //    var origin = ctx.Request.Headers["Origin"];
        //    var allowOrigin = !string.IsNullOrWhiteSpace(origin) ? origin : "*";
        //    ctx.Response.AddHeader("Access-Control-Allow-Origin", allowOrigin);
        //    ctx.Response.AddHeader("Access-Control-Allow-Headers", "*");
        //    ctx.Response.AddHeader("Access-Control-Allow-Credentials", "true");


        //    //filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "http://localhost:4200");
        //    //filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers", "*");
        //    //filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Credentials", "true");

        //    base.OnActionExecuting(filterContext);
        //}
    }
}
