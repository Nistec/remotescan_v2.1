﻿using RemoteController.Model;
using Newtonsoft.Json;
using Nistec.Channels;
using Nistec.IO;
using Nistec.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController
{
    public class ScanController
    {
        public static string ReadRequestFile(string filename)
        {
            return FileStreamHelper.ReadFile(filename);
        }

        public static ScanDocument ReadRequest(string jsonString)
        {
            ScanDocument doc = null;

            if (string.IsNullOrEmpty(jsonString))
            {
                LogListItems.Instance.AppendLog("Error: Request is Empty...");
                throw new ArgumentException("Incorrect Request");
            }
            else
            {
                //LogListItems.Instance.AppendLog("Info: Request Json: " + jsonString);

                doc = ScanDocument.Create(jsonString);
                if (doc == null)
                    throw new ArgumentException("Incorrect json");

            }
            doc.EnsureDocument();
            LogListItems.Instance.AppendLog("Info: ScanDocument created, ScanType: " + doc.ScanType);
            return doc;
        }

        public static ResultModel ExecRequest(string jsonString)
        {
            

            try
            {

                LogListItems.Instance.AppendLog("Info: New incoming request.", false);

                //doc= ReadRequest(jsonString);

                //if (ScanSettings.EnableService)
                //{
                //    if (doc != null)
                //        Task.Factory.StartNew(() => PipeClientExtension.SendOut(doc));
                //    else
                //        Task.Factory.StartNew(() => PipeClientExtension.SendOutScan());  
                //}
                //else
                //{
                //    ScanModel scan = new ScanModel();
                //    scan.ExecScan(doc);
                //}
                if (ScanSettings.IsTrayServiceWatcher)
                {
                    ScanDocument.SaveRequest(jsonString);
                    return new ResultModel() { Status = "ok" , Message="Request was saved"};
                }
                else
                {
                    ScanDocument doc = ReadRequest(jsonString);

                    ScanController scan = new ScanController(doc);
                    scan.ExecScan();
                    return new ResultModel() { Status = "ok", Message = "Request was executed" };
                }
            }
            catch (Exception ex)
            {
                string err = "Error: Scan " + ex.Message;
                LogListItems.Instance.AppendLog(err, true);
                //Netlog.Error(err);
                return new ResultModel() { Status = "error", Message = err };

            }
        }

        ScanDocument doc;

        public ScanController(string jsonString)
        {
            doc = ReadRequest(jsonString);
            //if (!doc.IsValid)
            //    doc.EnsureDocument();
        }

        public ScanController(ScanDocument sd)
        {
            doc = sd;
            //if (!doc.IsValid)
            //    doc.EnsureDocument();
        }

        public void ExecScan()
        {
            try
            {
                LogListItems.Instance.AppendLog("Info: ExecScan start.");

                if (doc == null)
                    throw new Exception("Invalid ScanDocument");

                doc.SaveToFile();

                //string scanType = doc.ScanParms.ScanType;
                //if (string.IsNullOrWhiteSpace(scanType))
                //    scanType = "naps2";

                //LogListItems.Instance.AppendLog("Info: ExecScan ScanSettings.");
                //LogListItems.Instance.AppendLog("Info: ExecScan scanType " + scanType);
                //LogListItems.Instance.AppendLog("Info: ExecScan filename " + filename);
                //LogListItems.Instance.AppendLog("Info: ExecScan path " + path);
                //LogListItems.Instance.AppendLog("Info: ExecScan docFilename " + docFilename);

                RunScanner();

                RunViewer();

                LogListItems.Instance.AppendLog("Info: ExecScan Completed.", false);
            }
            catch (Exception ex)
            {
                string err = "Error: ExecScan " + ex.Message + ", Trace: " + ex.StackTrace;
                LogListItems.Instance.AppendLog(err, true);
                Netlog.Error(err);
            }

        }

        public void RunScanner()
        {
            string exe = null;
            string naps_arguments = null;
            string scanType = doc.ScanType.ToLower();

            LogListItems.Instance.AppendLog("Info: Scan document using " + scanType, false);


            if (scanType == "naps2")
            {
                exe = ScanSettings.naps2_scan_exe_path;
                if (exe == null)
                {
                    throw new Exception("Invalid scanner file name");
                }
                if (ScanSettings.deleteFileBeforeNewScan)
                {
                    ScanSettings.ClearDocumentFolderBeforScan();
                }
                naps_arguments = "-o " + doc.ScanFilename;//  Path.Combine(path, filename);
                int waitForExit = ScanSettings.scan_wait_seconds;
                if (waitForExit > 0)
                    waitForExit = waitForExit * 1000;//toMiliseconds
                bool isSilentMode = ScanSettings.scan_in_silent_mode;


                // run NAPS2
                int exitCode = 0;
                if (ScanSettings.IsServiceMode)
                {
                    //Impersonation.ExecuteAppAsLoggedOnUser("naps2", null);
                    using (ProcessStarter ps = new ProcessStarter("naps2", exe, naps_arguments))
                    {
                        ps.Run(null);// Naps((uint)waitForExit);
                        exitCode = ps.WaitForExit();
                    }
                }
                else
                {
                    exitCode = WinProc.RunAppSilent("ExecScan naps2", exe, naps_arguments, isSilentMode, waitForExit);
                }

                LogListItems.Instance.AppendLog("Info: ExecScan exitCode: " + exitCode.ToString());

                //if(exitCode!=0)
                //{
                //    //LogListItems.Instance.AppendLog("Info: ExecScan naps2 has a problem.");
                //    return;
                //}
                //LogListItems.Instance.AppendLog("Info: ExecScan WinProc.RunApp.");
            }
            else if (scanType == "opentext")
            {
                exe = ScanSettings.opentext_scan_exe_path;
                if (exe == null)
                {
                    throw new Exception("Invalid scanner file name");
                }
                throw new Exception("opentext_scan not supported");
                //arguments = "-o " + filename;
            }
        }

        public void RunViewer()
        {
            LogListItems.Instance.AppendLog("Info: EnableEntScan: " + doc.ScanParms.EnableEntScan.ToString());

            if (doc.ScanParms.EnableEntScan)
            {
                var enterprise_exe = ScanSettings.enterprise_exe_path;
                if (string.IsNullOrEmpty(enterprise_exe))
                {
                    LogListItems.Instance.AppendLog("Invalid Enterprise scanner directory.", true);
                    return;
                }

                LogListItems.Instance.AppendLog("Info: Run Enterprise scanner.", false);
                // run enterprise scanner


                if (ScanSettings.IsServiceMode)
                {
                    using (ProcessStarter ps = new ProcessStarter("enterprise", enterprise_exe))
                    {
                        ps.RunEnterpriseScan();
                        //ps.WaitForExit();
                    }
                }
                else
                {
                    WinProc.ShowSessionApp(enterprise_exe, null, ProcessWindowStyle.Maximized);
                }

                //WinProc.RunApp(enterprise_exe, null, ProcessWindowStyle.Maximized, true);
            }
        }

        public void ShowSessionApp(IntPtr ptr)
        {
            LogListItems.Instance.AppendLog("Info: ShowSessionApp.", false);
            WinProc.ActivateApp(ptr, (int)ProcessWindowStyle.Maximized);
        }
    }
}
