﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using RemoteController.Model;
using Newtonsoft.Json;
using Nistec.Logging;
using System.IO;
using System.Web;
using Nistec.Channels;
using System.Web.Http.Cors;
using System.Threading.Tasks;

namespace RemoteController
{
    [AllowCrossSite]
    [EnableCors(origins:"*",headers:"*", methods:"get,post")]
    public class OpsApiController : ApiController
    {

        
       
        //sample
        //ScanDocument d = new ScanDocument()
        //{
        //    ScanParms = new ScanParms()
        //    {
        //         EnableEntScan=true,
        //         ScanType = "Naps2"
        //    },
        //    ApplicationParams = new ApplicationParams()
        //    {
        //         DepartmentCode="123",
        //         DepartmentName = "abc",
        //         InstituteCode = "123",
        //         IntituteName = "abc",
        //         PatientFirstName = "abc",
        //         PatientID = "12345678",
        //         PatientIDType = 1,
        //         PatientLastName = "family",
        //         SystemCode = "123",
        //         SystemName = "abc",
        //         UserName = "user"

        //    }
        //};
        //jsonString = JsonConvert.SerializeObject(d);
        //var doc = JsonConvert.DeserializeObject<ScanDocument>(jsonString);

       //protected void Validate(HttpRequestMessage request)
       //{

       //}

       [HttpPost]
       [HttpGet]
      // [EnableCors(origins: "https://localhost:4441/", headers: "*", methods: "*")]
        public HttpResponseMessage Scan(HttpRequestMessage request)
       {
           ScanDocument doc = null;
           string jsonString = null;
           ResultModel result=null;

            try
           {
 

               LogListItems.Instance.AppendLog("Info: New incoming request.", false);

               var headers = request.Headers;

               //string Origin=null;
               //IEnumerable<string> values;
               //if (headers.TryGetValues("Origin", out values))
               //{
               //    Origin = values.FirstOrDefault();
               //}
               //Netlog.Info("Info: Request Headers: " + headers.ToString() + "\n Origin: " + Origin);


               if (!string.IsNullOrEmpty(request.RequestUri.Query))
               {
                   //query has json argument

                   var qs = request.RequestUri.ParseQueryString();

                   if (qs.Count > 0)
                   {
                       string query = qs.Get(0);
                       jsonString = HttpUtility.UrlDecode(query);

                        result= ScanController.ExecRequest(jsonString);

                       //LogListItems.Instance.AppendLog("Info: Request Query: " + jsonString);
                       // if (!string.IsNullOrEmpty(jsonString))
                       // {
                       //     doc = JsonConvert.DeserializeObject<ScanDocument>(jsonString);
                       //     if (doc == null)
                       //         throw new ArgumentException("Incorrect json");
                       // }
                    }
               }
               else
               {
                   var httpContent = request.Content;
                   if (httpContent != null)
                   {
                       jsonString = httpContent.ReadAsStringAsync().Result;

                        result = ScanController.ExecRequest(jsonString);

                        //LogListItems.Instance.AppendLog("Info: Request Json: " + jsonString);

                        // if (!string.IsNullOrEmpty(jsonString))
                        // {
                        //     doc = JsonConvert.DeserializeObject<ScanDocument>(jsonString);
                        //     if (doc == null)
                        //         throw new ArgumentException("Incorrect json");
                        // }
                    }
               }


                //if (ScanSettings.EnableService)
                //{
                //    if (doc != null)
                //        Task.Factory.StartNew(() => PipeClientExtension.SendOut(doc));
                //    else
                //        Task.Factory.StartNew(() => PipeClientExtension.SendOutScan());  
                //}
                //else
                //{
                //    ScanModel scan = new ScanModel();
                //    scan.ExecScan(doc);
                //}

                //ScanModel scan = new ScanModel();
                //scan.ExecScan(doc);

                //string content = JsonConvert.SerializeObject(new ResultModel() { Status = "ok"});

                if (result == null)
                    result = new ResultModel() { Status = "error", Message = "Unexcpected error" };

                string content = JsonConvert.SerializeObject(result);

                var response = new HttpResponseMessage()
                {
                    Content = new StringContent(content+";", Encoding.UTF8, "text/plain")// "text/plain")//, "application/json")
               };

               response.Headers.Add("Access-Control-Allow-Origin", "*");
               return response;

           }
           catch (Exception ex)
           {
               string err = "Error: Scan " + ex.Message;
               LogListItems.Instance.AppendLog(err, true);
               //Netlog.Error(err);
               string content = JsonConvert.SerializeObject(new ResultModel() { Status = "error", Message = err });
               return new HttpResponseMessage()
               {
                   Content = new StringContent(content, Encoding.UTF8, "text/plain")//, "application/json")
               };
           }
       }

        /*
         [HttpPost]
         [HttpGet]
         public HttpResponseMessage Scan(HttpRequestMessage request)
         {
             ScanDocument doc = null;
             string jsonString = null;
             try
             {
                 LogListItems.Instance.AppendLog("Info: New incoming request.", false);

                 if (!string.IsNullOrEmpty(request.RequestUri.Query))
                 {
                     var qs = request.RequestUri.ParseQueryString();

                     if (qs.Count == 1) //query has json argument
                     {
                         string query = qs.Get(0);
                         jsonString = HttpUtility.UrlDecode(query);
                         LogListItems.Instance.AppendLog("Info: Request Query: " + jsonString);
                         if (!string.IsNullOrEmpty(jsonString))
                             doc = JsonConvert.DeserializeObject<ScanDocument>(jsonString);
                     }
                     else // query string arguments
                     {
                         LogListItems.Instance.AppendLog("Info: Request Query args: " + request.RequestUri.Query);
                         doc = ScanDocumentContext.ReadQuery(qs);
                     }
                 }
                 else
                 {
                     var httpContent = request.Content;
                     if (httpContent != null)
                     {
                         jsonString = httpContent.ReadAsStringAsync().Result;
                         LogListItems.Instance.AppendLog("Info: Request Json: " + jsonString);

                         if (!string.IsNullOrEmpty(jsonString))
                             doc = JsonConvert.DeserializeObject<ScanDocument>(jsonString);
                     }
                 }

                 if (ScanSettings.running_mode == "service")
                 {
                     if (doc != null)
                         PipeClientExtension.SendOut(doc);
                     else
                         PipeClientExtension.SendOutScan();
                 }
                 else
                 {
                     ScanModel scan = new ScanModel();
                     scan.ExecScan(doc);
                 }

                 string content = JsonConvert.SerializeObject(new ResultModel() { Status = "ok" });

                 return new HttpResponseMessage()
                    {
                        Content = new StringContent(content, Encoding.UTF8,"text/plain")//, "application/json")
                    };
             }
             catch (Exception ex)
             {
                 string err = "Error: Scan " + ex.Message;
                 LogListItems.Instance.AppendLog(err, true);
                 //Netlog.Error(err);
                 string content = JsonConvert.SerializeObject(new ResultModel() { Status = "error", Reason = err });
                 return new HttpResponseMessage()
                 {
                     Content = new StringContent(content, Encoding.UTF8 , "text/plain")//, "application/json")
                 };
             }
         }
        */
    }
}
