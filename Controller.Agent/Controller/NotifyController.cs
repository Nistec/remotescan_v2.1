﻿using System;
using Controller;
using RemoteController.Model;
using Microsoft.Win32;
using Nistec.Channels;
using static System.Net.Mime.MediaTypeNames;

namespace RemoteController
{
    public class NotifyController
    {
        AgentManager sm;
        ConfigFileWatcher cfWatcher;
        RemotePipeServer pipeserver;
        IncomingFileWatcher incomingFileWatcher;
        public readonly LogListItems logItem;

        /// <summary>
        /// Sets up the popup window and instantiates the notify icon
        /// </summary>
        public NotifyController()
        {
            logItem = LogListItems.Instance;
            //logItem.LogChanged += Instance_LogChanged;


            SystemEvents.SessionEnding += SystemEvents_SessionEnding;
            SystemEvents.SessionEnded += SystemEvents_SessionEnded;
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;


            //Start();

            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            logItem.AppendLog("MainNotifyWindow start, CurrentUser:  " + CurrentUser);
        }

        public void AppendLog(string message, bool isError = false)
        {
            logItem.AppendLog(message, isError);
        }


        //mode: 1: startup , -1 :delete
        public void SetStartup(int mode)
        {
            if (ScanSettings.enable_autostart)
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string AppName = ScanSettings.AppName;

                if (mode == 1)
                    rk.SetValue(AppName, location);
                else if (mode == -1)
                    rk.DeleteValue(AppName, false);
            }
            ScanSettings.CreateLogFolder();
        }

        public void Start()
        {
            try
            {
                //SetStartup(1);

                if (ScanSettings.IsServiceMode)
                    return;


                //if (sm == null && ScanSettings.EnableTray)
                //{
                //    sm = new Controller.AgentManager();
                //    sm.Start();
                //    ScanSettings.CreateDocumentFolder();
                //}
                if (ScanSettings.EnableService)
                {

                    if (ScanSettings.EnableTray)
                    {
                        if (ScanSettings.tray_service_mode == "pipe")
                        {
                            if (pipeserver == null)
                            {
                                pipeserver = new RemotePipeServer();
                                pipeserver.Start();
                            }
                            logItem.AppendLog("RemotePipeServer started..." );
                        }
                        else if (ScanSettings.tray_service_mode == "watcher")
                        {
                            ScanSettings.CreateIncomingDocumentFolder();

                            if (incomingFileWatcher == null)
                            {
                                incomingFileWatcher = new IncomingFileWatcher(ScanSettings.incoming_doc_path, "*.*", ScanSettings.request_filename);
                                incomingFileWatcher.FileChangedAction = (request) =>
                                {
                                    //string request = ScanController.ReadRequestFile(e.Name);
                                    //ScanController.DoScan(request);

                                    logItem.AppendLog("Incoming new request: " + request);

                                    ScanController scan = new ScanController(request);
                                    scan.ExecScan();
                                };

                                logItem.AppendLog("IncomingFileWatcher started...");

                                //incomingFileWatcher.Init(ScanSettings.document_path, (string filename) =>
                                //{
                                //    string request=ScanController.ReadRequestFile(filename);
                                //    ScanController.DoScan(request);

                                //});
                                //incomingFileWatcher.Start();
                            }
                        }
                    }

                    if (ScanSettings.autostart_remote_service)
                    {
                        var rsm = new RemoteServiceManager();
                        if (rsm.IsServiceInstalled())
                        {
                            if (!rsm.IsServiceStarted())
                                rsm.DoServiceCommand(ServiceCmd.Start);
                        }
                    }

                    //if (MessageBox.Show("האם להפעיל את שירות ההדפסות", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    //{
                    //    var rsm = new RemoteServiceManager();
                    //    if (!rsm.IsServiceInstalled())
                    //        rsm.DoServiceCommand(ServiceCmd.Install);
                    //    if (!rsm.IsServiceStarted())
                    //        rsm.DoServiceCommand(ServiceCmd.Start);
                    //}
                }
                else
                {
                    if (sm == null && ScanSettings.EnableTray)
                    {
                        sm = new AgentManager();
                        sm.Start();
                        ScanSettings.CreateDocumentFolder();
                    }
                }

                if (cfWatcher == null)
                {
                    cfWatcher = new ConfigFileWatcher(ConfigFileWatcher.RemoteScanTrayFile);
                    cfWatcher.FileChanged += cfWatcher_FileChanged;
                    cfWatcher.Start(true);
                }

                logItem.AppendLog("NotifyControoler started...");
            }
            catch (Exception ex)
            {
                logItem.AppendLog("Error in tray Start: " + ex.Message, true);
            }
        }

        void cfWatcher_FileChanged(object sender, System.IO.FileSystemEventArgs e)
        {
            ScanSettings.Reset();
            if (sm != null)
                sm.Restart();
        }



        public void StartAgent()
        {
            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            logItem.AppendLog("OnSessionChange SessionLogon, CurrentUser:  " + CurrentUser);

            if (sm == null && ScanSettings.EnableTray)
            {
                sm = new AgentManager();
                sm.Start();
            }
        }
        public void StopAgent()
        {
            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            logItem.AppendLog("OnSessionEnded SessionLogoff, CurrentUser:  " + CurrentUser);
            if (sm != null)
            {
                sm.Stop();
                sm = null;
            }
        }

        #region SystemEvents

        void SystemEvents_SessionEnding(object Sender, SessionEndingEventArgs e)
        {
            StopAgent();
        }
        void SystemEvents_SessionEnded(object Sender, SessionEndedEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionEndReasons.Logoff:
                case SessionEndReasons.SystemShutdown:
                default:
                    StopAgent();
                    break;
            }
        }

        public string CurrentUser { get; private set; }
        void SystemEvents_SessionSwitch(object Sender, SessionSwitchEventArgs e)
        {
            //logItem.AppendLog("OnSessionChange SessionLogon Start...");


            try
            {
                switch (e.Reason)
                {
                    case SessionSwitchReason.SessionLock:
                        break;
                    case SessionSwitchReason.SessionUnlock:
                    case SessionSwitchReason.ConsoleConnect:
                    case SessionSwitchReason.RemoteConnect:
                        break;
                    case SessionSwitchReason.SessionLogoff:
                        //logItem.AppendLog("OnSessionChange SessionLogoff, CurrentUser:  " + CurrentUser);
                        //WinProc.KillProcess("RemoteScan.Proc");

                        StopAgent();

                        break;
                    case SessionSwitchReason.SessionLogon:
                        //Svc.SwitchChildProc();

                        StartAgent();

                        //string location = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        //string filename = Path.Combine(location, "RemoteScan.Proc.exe");
                        //WinProc.RunAppAs("RemoteScan.Proc", filename, null, null, null);

                        //WinProc.TerminateProcessExceptCurrentSession("RemoteScan.Proc");

                        //ApplicationLauncher.CreateProcessInConsoleSession("","", true);

                        //logItem.AppendLog("OnSessionChange SessionLogon Completed");
                        break;
                    case SessionSwitchReason.RemoteDisconnect:
                        break;
                }
            }
            catch (Exception ex)
            {
                logItem.AppendLog("OnSessionChange Error: " + ex.Message, true);
            }
            //changeDescription.SessionId
        }

        #endregion

        public void Stop(bool stopService)
        {

            if (sm != null)
            {
                sm.Stop();
                sm = null;
            }
            if (ScanSettings.EnableService)
            {
                if (stopService)
                {
                    var rsm = new RemoteServiceManager();
                    rsm.DoServiceCommand(ServiceCmd.Stop);
                }
            }

        }

        public void Quit()
        {
            if (sm != null)
            {
                sm.Stop();
                sm = null;
            }
            if (ScanSettings.EnableService)
            {
                if (pipeserver != null)
                {
                    pipeserver.Stop();
                    pipeserver = null;
                }
                //if (MessageBox.Show("האם לעצור את שירות ההדפסות", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                //{
                //    var rsm = new RemoteServiceManager();
                //    rsm.DoServiceCommand(ServiceCmd.Stop);
                //}
            }
            if (cfWatcher == null)
            {
                cfWatcher.Stop();
                cfWatcher = null;
            }

            //SetStartup(-1);
        }

        public void Close()
        {
            if (sm != null)
            {
                sm.Stop();
                sm = null;
            }
            if (ScanSettings.EnableService)
            {
                if (pipeserver != null)
                {
                    pipeserver.Stop();
                    pipeserver = null;
                }
                //if (MessageBox.Show("האם לעצור את שירות ההדפסות", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                //{
                //    var rsm = new RemoteServiceManager();
                //    rsm.DoServiceCommand(ServiceCmd.Stop);
                //}
            }
            if (cfWatcher == null)
            {
                cfWatcher.Stop();
                cfWatcher = null;
            }

            //SetStartup(-1);
        }
    }
}