//#define SERVICE

using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Configuration;

namespace Controller
{
	public class Service1 : System.ServiceProcess.ServiceBase
	{
        AgentManager Svc;

        public Service1()
        {
            Svc = new AgentManager();
        }

		protected override void OnStart(string[] args)
		{
            Svc.Start(); 
		}
 
		protected override void OnStop()
		{
			Svc.Stop();
		}

        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);
            if (Svc == null)
                return;
            //Svc.DoCommand(command);
        }

        //protected override void OnShutdown()
        //{
        //    Svc.Stop();
        //}
      
	}

	

}
