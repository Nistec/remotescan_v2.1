﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;
using Nistec.Logging;
using System.Web.Http.SelfHost;
using System.Web.Http;
using System.Configuration;
using RemoteController.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Win32;

namespace Controller
{

    public class AgentManager
    {
        HttpSelfHostServer apiServer;

        bool _loaded = false;
        bool EnableWebApiServer;
        bool IsSsl;
        string CurrentUser;

        public bool Loaded
        {
            get { return _loaded; }
        }

        public AgentManager()
        {

            EnableWebApiServer = true;
            IsSsl = ScanSettings.is_ssl;// ConfigurationManager.AppSettings["is_ssl"] == "true";

            CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            Netlog.Debug("CurrentUser: " + CurrentUser);

            if (ScanSettings.IsServiceMode)
                SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;

        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            SwitchChildProc();
        }

        

        public void Start()
        {
            Thread Th = new Thread(new ThreadStart(InternalStart));
            Th.Start();
        }


        private void ApiStartSsl()
        {

            //netsh http add urlacl url=http://+:80/MyUri user=DOMAIN\user
            string port = ScanSettings.http_Port;// ConfigurationManager.AppSettings["http_Port"];
            string address = "https://localhost:" + port;

            if (ScanSettings.is_service)
                Netlog.Debug("Info: Scan api start : " + address);
            else
                LogListItems.Instance.AppendLog("Info: Scan api start : " + address, false);


            var config = new WindowsSelfHostConfiguration(address);

            config.MessageHandlers.Add(new CustomHeaderHandler());

            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            //CertificateSettings.SetCertificate(StoreName.Root);

            //apiServer = new HttpSelfHostServer(config);
            //apiServer.OpenAsync()
            //.ContinueWith(task =>
            //    {
            //        //var client = new ExampleClientHelper(baseUri);
            //        //var values = client.GetValues();


            //        X509Store store = new X509Store("My", StoreLocation.LocalMachine);
            //        store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            //        var x509=new X509Certificate2();
            //        x509.Thumbprint="236D7D4AD95753B8F22D3781D61AACB45518E1B5";

            //        //store.Certificates.Add(new X509Certificate2(){ }
            //        //X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
            //        //foreach (X509Certificate2 x509 in collection)
            //        //{
            //        //    if (x509.Thumbprint == "236D7D4AD95753B8F22D3781D61AACB45518E1B5")
            //        //    {
            //        //        //client.ClientCredentials.ClientCertificate.SetCertificate(
            //        //        //    x509.SubjectName.Name, store.Location, StoreName.My);
            //        //    }
            //        //}
            //        // then test the response
            //        //Assert.AreEqual("value1", values.ElementAt(0));
            //        //Assert.AreEqual("value2", values.ElementAt(1));
            //    })
            //.Wait();


            apiServer = new HttpSelfHostServer(config);
            apiServer.OpenAsync().Wait();

            if (ScanSettings.is_service)
                Netlog.Debug("Ops api server started!");
            else
                LogListItems.Instance.AppendLog("Info: Scan api started!", false);
        }

        private void ApiStart()
        {

            //netsh http add urlacl url=http://+:80/MyUri user=DOMAIN\user
            string port = ScanSettings.http_Port;// ConfigurationManager.AppSettings["http_Port"];
            string address = "http://localhost:" + port;

            LogListItems.Instance.AppendLog("Info: Scan api start : " + address, false);

            var config = new HttpSelfHostConfiguration(address);//"http://localhost:8082");
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            apiServer = new HttpSelfHostServer(config);
            apiServer.OpenAsync().Wait();


            if (ScanSettings.is_service)
                Netlog.Debug("Ops api server started!");
            else
                LogListItems.Instance.AppendLog("Info: Scan api started!", false);
        }

        private void ApiStop()
        {
            if (apiServer != null)
            {
                apiServer.CloseAsync();

                if (ScanSettings.is_service)
                    Netlog.Debug("Ops api server stoped!");
                else
                    LogListItems.Instance.AppendLog("Info: Scan api stoped!", false);
            }
        }
        private void InternalStart()
        {
            try
            {
                Netlog.Debug(Settings.ServiceName + " start...");

                //SystemEvents.SessionSwitch += (sender, args) =>
                //{
                //    SwitchChildProc();
                //};

                if (ScanSettings.IsServiceMode)
                    SwitchChildProc();

                if (EnableWebApiServer)
                {
                    if (IsSsl)
                        ApiStartSsl();
                    else
                        ApiStart();
                }

                Netlog.Debug(Settings.ServiceName + " started!");
            }
            catch (Exception ex)
            {
                Netlog.Exception(Settings.ServiceName + " ", ex, true, true);
                LogListItems.Instance.AppendLog("Error: Scan api Exception " + ex.Message, false);
            }
        }

        public void SwitchChildProc()
        {
            //CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //Netlog.Debug("SystemEvents.SessionSwitch, CurrentUser:  " + CurrentUser);

            //WinProc.KillProcess("RemoteScan.Proc");
            //string location = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            //string filename=Path.Combine(location,"RemoteScan.Proc.exe");
            //WinProc.RunAppAs("RemoteScan.Proc", filename, null,"nissimtr",null);
            //Netlog.Debug("SystemEvents.SessionSwitch Completed");
        }

        public void Restart()
        {
            try
            {
                Netlog.Debug(Settings.ServiceName + " restart...");

                ApiStop();

                if (EnableWebApiServer)
                {
                    if (IsSsl)
                        ApiStartSsl();
                    else
                        ApiStart();
                }

                Netlog.Debug(Settings.ServiceName + " restarted!");
            }
            catch (Exception ex)
            {
                Netlog.Exception(Settings.ServiceName + " ", ex, true, true);
                LogListItems.Instance.AppendLog("Error: Restart Scan api Exception " + ex.Message, false);
            }
        }
        public void Stop()
        {
            Netlog.Debug(Settings.ServiceName + " stop...");

            ApiStop();

            if (ScanSettings.IsServiceMode)
                WinProc.KillProcess("RemoteScan.Proc");

            Netlog.Debug(Settings.ServiceName + " stoped.");
        }

    }

}
