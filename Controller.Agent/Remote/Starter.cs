﻿using Controller.Agent.Model;
using Microsoft.Win32;
using Nistec.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Agent.Remote
{
    public class Starter
    {
        AgentManager sm;
        ConfigFileWatcher cfWatcher;
        RemotePipeServer pipeserver;
        //LogListItems logItem;

        public void Start()
        {
            try
            {
                SetStartup(1);
                ScanSettings.CreateLogFolder();
                ScanSettings.CreateDocumentFolder();


               
                if (ScanSettings.running_mode.Contains("service"))
                {
                    if (pipeserver == null)
                    {
                        pipeserver = new RemotePipeServer();
                        pipeserver.Start();
                    }
                    //if (MessageBox.Show("האם להפעיל את שירות ההדפסות", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    //{
                        var rsm = new RemoteServiceManager();
                        if (!rsm.IsServiceInstalled())
                            rsm.DoServiceCommand(ServiceCmd.Install);
                        if (!rsm.IsServiceStarted())
                            rsm.DoServiceCommand(ServiceCmd.Start);
                    //}
                }

                if (sm == null && ScanSettings.running_mode.Contains("tray"))
                {
                    sm = new AgentManager();
                    sm.Start();
                }

                if (cfWatcher == null)
                {
                    cfWatcher = new ConfigFileWatcher(ConfigFileWatcher.RemoteScanTrayFile);
                    cfWatcher.FileChanged += cfWatcher_FileChanged;
                    cfWatcher.Start(true);
                }
            }
            catch (Exception ex)
            {
                LogListItems.Instance.AppendLog("Error in tray Start: " + ex.Message, true);
            }
        }

        void cfWatcher_FileChanged(object sender, System.IO.FileSystemEventArgs e)
        {
            ScanSettings.Reset();
            if (sm != null)
                sm.Restart();
        }

        //mode: 1: startup , -1 :delete
        void SetStartup(int mode)
        {
            if (ScanSettings.enable_autostart)
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string AppName = ScanSettings.AppName;

                if (mode == 1)
                    rk.SetValue(AppName, location);
                else if (mode == -1)
                    rk.DeleteValue(AppName, false);
            }
        }

    }
}
