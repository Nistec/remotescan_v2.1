﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.SelfHost;
using System.Web.Http.SelfHost.Channels;

namespace Controller
{
    public class NtlmSelfHostConfiguration : HttpSelfHostConfiguration
    {


        public NtlmSelfHostConfiguration(string baseAddress)
            : base(baseAddress)
        {
            this.EnableCors();
        }

        //private ICorsPolicyProvider EnableCorsAttribute(string v, string headers, string methods)
        //{
        //    throw new NotImplementedException();
        //}

        public NtlmSelfHostConfiguration(Uri baseAddress)
            : base(baseAddress)
        {
            this.EnableCors(new EnableCorsAttribute("*", headers: "*", methods: "*"));
        }

        protected override BindingParameterCollection OnConfigureBinding(HttpBinding httpBinding)
        {
            httpBinding.Security.Mode = HttpBindingSecurityMode.TransportCredentialOnly;
            httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            return base.OnConfigureBinding(httpBinding);
        }
    }

    public class CustomHeaderHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken)
                .ContinueWith((task) =>
                {
                    HttpResponseMessage response = task.Result;
                    response.Headers.Add("Access-Control-Allow-Origin", "*");
                    return response;
                });
        }
    }
    public class WindowsSelfHostConfiguration : HttpSelfHostConfiguration
    {
        public WindowsSelfHostConfiguration(string baseAddress)
            : base(baseAddress)
        {
            this.EnableCors(new EnableCorsAttribute("*", headers: "*", methods: "*"));
        }

        public WindowsSelfHostConfiguration(Uri baseAddress)
            : base(baseAddress)
        {
            this.EnableCors(new EnableCorsAttribute("*", headers: "*", methods: "*"));
          
            //X509Store store = new X509Store("My", StoreLocation.LocalMachine);
            //store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            //X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
            //foreach (X509Certificate2 x509 in collection)
            //{
            //    if (x509.Thumbprint == "236D7D4AD95753B8F22D3781D61AACB45518E1B5")
            //    {

            //        base.ClientCredentialType = HttpClientCredentialType.Windows;
            //        base.X509CertificateValidator=

            //        client.ClientCredentials.ClientCertificate.SetCertificate(
            //            x509.SubjectName.Name, store.Location, StoreName.My);
            //    }
            //}
        }

        protected override BindingParameterCollection OnConfigureBinding(HttpBinding httpBinding)
        {
            httpBinding.Security.Mode = HttpBindingSecurityMode.Transport;
            httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            return base.OnConfigureBinding(httpBinding);
        }
       
    }
    public class NoSelfHostConfiguration : HttpSelfHostConfiguration
    {
        public NoSelfHostConfiguration(string baseAddress)
            : base(baseAddress)
        {
            this.EnableCors();
        }

        public NoSelfHostConfiguration(Uri baseAddress)
            : base(baseAddress)
        {
            this.EnableCors();
        }

        protected override BindingParameterCollection OnConfigureBinding(HttpBinding httpBinding)
        {
            httpBinding.Security.Mode = HttpBindingSecurityMode.Transport;
            //httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            return base.OnConfigureBinding(httpBinding);
        }
    }


}
