﻿using RemoteController.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Web.Http.SelfHost;
using System.Web.Http.SelfHost.Channels;

namespace Controller
{
    public class CertificateSettings
    {

        public static void SetCertificate(StoreName stor= StoreName.Root)
        {

            var tumbprint = ScanSettings.CertificateThumbprint;
            bool found = IsCertificateExists(tumbprint, stor);

            if(!found)
            {
                var filename = ScanSettings.CertificateFilename;
                var password = ScanSettings.CertificatePassword;
                InstallCert(filename, password, stor);
            }
        }

        public static bool IsCertificateExists(string tumbprint, StoreName stor = StoreName.Root)
        {
            if (tumbprint == null || tumbprint == "")
                return false;
            tumbprint = tumbprint.ToUpper();
            bool found = false;
            X509Store store = new X509Store(stor, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            foreach (var cert in store.Certificates)
            {
                //Console.WriteLine("{0}--{1}", cert.SubjectName.Name, "");
                if(cert.Thumbprint==tumbprint)
                {
                    found = true;
                    break;
                }
            }
            store.Close();
            return found;
        }
    
        public static void InstallCert(byte[] tumbprint, StoreName storeName= StoreName.Root)
        {
            X509Store store = new X509Store(storeName, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadWrite);
            
            X509Certificate2 cert = new X509Certificate2(tumbprint);
            
            store.Add(cert);

            store.Close();
        }

        public static void InstallCert(string filename, string password, StoreName storeName = StoreName.Root)
        {
            X509Store store = new X509Store(storeName, StoreLocation.LocalMachine);
            
            store.Open(OpenFlags.ReadWrite);
            
            X509Certificate2 cert = new X509Certificate2(filename, password);
            
            store.Add(cert);
            
            store.Close();
        }

        public static void ListCert(StoreName storeName = StoreName.Root)
        {

            X509Store store = new X509Store(storeName, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadOnly);

            foreach (var cert in store.Certificates)
            {
                Console.WriteLine("{0}--{1}", cert.SubjectName.Name, "");
            }


            store.Close();
        }

        public static void RemoveCert(string CertName,StoreName storeName = StoreName.Root)
        {
            X509Store store = new X509Store(storeName, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadWrite);

            X509Certificate2 testcert = store.Certificates.Find(X509FindType.FindBySubjectName, CertName, false)[0];

            if (testcert != null)
            {
                store.Remove(testcert);
            }

            store.Close();
        }

    }
}
