//#define SERVICE

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Threading;
using Nistec.Logging;


namespace Controller
{
    public class Program
    {

#if (SERVICE)

        static void Main(string[] args)
        {

            try
            {

                if (args.Length > 0)
                {
                    //Install service
                    if (args[0].Trim().ToLower() == "/i")
                    { 
                        System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/i", Assembly.GetExecutingAssembly().Location }); 
                    }

                    //Uninstall service                 
                    else if (args[0].Trim().ToLower() == "/u")
                    { 
                        System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location }); 
                    }
                    //run as console application
                    else if (args[0].Trim().ToLower() == "/c")
                    {
                        ServiceManager SVC = new ServiceManager();
                        SVC.Start();
                        Console.WriteLine("Server Started as console");
                        Console.ReadKey();
                    }
                }
                else
                {

                    Console.WriteLine("Server Started...");

                    System.ServiceProcess.ServiceBase[] ServicesToRun;
                    ServicesToRun = new System.ServiceProcess.ServiceBase[] { new Service1() };
                    System.ServiceProcess.ServiceBase.Run(ServicesToRun);
                }
            }
            catch (Exception ex)
            {
                Netlog.Exception(Settings.ServiceName, ex, true, true);
            }
        }

#else
        static void Main(string[] args)
        {
            ServiceManager SVC = new ServiceManager();// (RunMode.Console);

            try
            {
   
                //int applicatinId = ControllerSettings.ApplicationId;
                //Controllerr.DoController(ControllerLog.Write(ControllerMode.Debug, "test log 1", applicatinId));
                //Controllerr.DoController(ControllerLog.Write(ControllerMode.Debug, "test log 2", applicatinId));
                //Controllerr.DoController(ControllerLog.Write(ControllerMode.Debug, "test log 3", applicatinId));

                SVC.Start();
                Console.WriteLine("Server Started...");
                Thread.Sleep(1000);

                //Console.WriteLine("Server quit...");
                Console.ReadLine();
            }
            catch (Exception ex)
            {

                Console.Write("*****************ERROR**************");
                Console.Write(ex.Message);
                Console.Write(ex.StackTrace);
                Console.ReadLine();

            }
        }

#endif

#if(false)
 static void Main(string[] args)
        {
            ServiceManager SVC = new ServiceManager();// (RunMode.Console);

            //try
            //{

            //    //int applicatinId = ControllerSettings.ApplicationId;
            //    //Controllerr.DoController(ControllerLog.Write(ControllerMode.Debug, "test log 1", applicatinId));
            //    //Controllerr.DoController(ControllerLog.Write(ControllerMode.Debug, "test log 2", applicatinId));
            //    //Controllerr.DoController(ControllerLog.Write(ControllerMode.Debug, "test log 3", applicatinId));

            //    SVC.Start();

            //    //ManagerConfig config = Config.CreateManagerConfig();
            //    //manager.Start(McLock.Lock.ValidateLock(),true);

            //    Console.WriteLine("Server Started...");
            //}
            //catch (Exception ex)
            //{

            //    Console.Write("*****************ERROR**************");
            //    Console.Write(ex.Message);
            //    Console.Write(ex.StackTrace);
            //    Console.ReadLine();

            //}
            //Console.TreatControlCAsInput=true;
            string cmd = null;
            string subcmd = null;
            int icommand = 0;
            bool started = false;

            SVC.Start();
            Console.WriteLine("Server Started...");
            Thread.Sleep(1000);

            Console.WriteLine("Command...");
            cmd = Console.ReadLine();

            while (true)
            {
                try
                {
                    //if (!started)
                    //{
                    //    SVC.Start();
                    //    Console.WriteLine("Server Started...");
                    //    started = true;
                    //}

                    Console.WriteLine("Command...");
                    cmd = Console.ReadLine();
                    if (cmd.Length > 0)
                    {
                        string[] c = cmd.Split(' ');
                        subcmd = cmd.Replace(c[0], "").Trim();
                        cmd = c[0].Trim();
                    }

                    if (cmd == "start")
                    {
                        if (SVC.Started)
                        {
                            Console.WriteLine("ServiceController allready started");
                            goto exit_label;
                        }
                        SVC.Start();
                        Console.WriteLine("Server Started...");
                    }
                    else if (cmd == "cmd")
                    {
                        icommand = (int)EnumExtension.Parse<ClusterCmd>(subcmd);
                        if (icommand > 255 || icommand < 128)
                        {
                            icommand = 0;
                            Console.WriteLine("ServiceController incorrecrt command :{0}", subcmd);
                            goto exit_label;
                        }
                        if (SVC == null || SVC.Started == false)
                        {
                            Console.WriteLine("ServiceController is invalid");
                            goto exit_label;
                        }
                        SVC.DoCommand(icommand);
                        Console.WriteLine("ExecuteCommand completed :{0}", icommand);
                    }
                    else if (cmd == "exec")
                    {
                        icommand = 0;
                        int.TryParse(subcmd, out icommand);
                        if (icommand > 255 || icommand < 128)
                        {
                            icommand = 0;
                            Console.WriteLine("ServiceController incorrecrt command :{0}", subcmd);
                            goto exit_label;
                        }
                        if (SVC == null || SVC.Started == false)
                        {
                            Console.WriteLine("ServiceController is invalid");
                            goto exit_label;
                        }
                        SVC.DoCommand(icommand);
                        Console.WriteLine("ExecuteCommand completed :{0}", icommand);
                    }
                    else if (cmd == "help")
                    {
                        string help =
                           "Service commander commands are:\r\n"
                            //+ "/open [service name]: connect to service controller\r\n"
                           + "/exec [command number]: execute command between 128 to 255\r\n"
                           + "/cmd [command enum]: execute command name\r\n"
                            //+ "/dispose: dispose the current service controller\r\n"
                           + "/quit: quit commander\r\n"
                           + "/help: print commander help\r\n"
                           + "Cmd: \r\n" + Strings.ArrayToString(Enum.GetNames(typeof(ClusterCmd)), '\n', false);
                        Console.WriteLine(help);
                    }
                    else if (cmd == "quit")
                    {
                        SVC.Stop();
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Command not supported");
                    }
                exit_label:
                    subcmd = "";// Console.WriteLine("");
                }
                catch (InvalidOperationException oex)
                {
                    Console.WriteLine("Error :{0}", oex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error :{0}", ex.Message);
                }
                finally
                {
                    //Console.WriteLine("...");
                }
            }
            Console.WriteLine("Server quit...");
        }
#endif

    }
}
