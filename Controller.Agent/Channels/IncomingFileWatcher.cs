﻿using Nistec.Channels;
using Nistec.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
namespace Nistec.Channels
{

   public class IncomingFileWatcher
   {

       public Action<string> FileAction { get; set; }
       public bool Initilaized { get; private set; }

       SysFileWatcher cfWatcher;

       public IncomingFileWatcher()
       {
           Initilaized = false;
           //this.CanHandleSessionChangeEvent = true;
           //Svc = new AgentManager();
       }

       public void Init(string path, Action<string> action)
       {

           if (cfWatcher != null)
               return;
           FileAction = action;
           cfWatcher = new SysFileWatcher(path, "*.json");// ConfigFileWatcher.RemoteScanAgentFile);
           cfWatcher.FileChanged += cfWatcher_FileChanged;
           Initilaized = true;
       }

       void cfWatcher_FileChanged(object sender, FileSystemEventArgs e)
       {
           if (FileAction != null)
               FileAction(e.Name);

           //ScanSettings.Reset();
           //if (Svc != null)
           //    Svc.Restart();
       }

       //public void Start()
       //{
       //    cfWatcher..Start(true);
       //}

       //public void Stop()
       //{
       //    cfWatcher.Stop();
       //}
   }
*/

//licHeader
//===============================================================================================================
// System  : Nistec.Lib - Nistec.Lib Class Library
// Author  : Nissim Trujman  (nissim@nistec.net)
// Updated : 01/07/2015
// Note    : Copyright 2007-2015, Nissim Trujman, All rights reserved
// Compiler: Microsoft Visual C#
//
// This file contains a class that is part of nistec library.
//
// This code is published under the Microsoft Public License (Ms-PL).  A copy of the license should be
// distributed with the code and can be found at the project website: http://nistec.net/license/nistec.cache-license.txt.  
// This notice, the author's name, and all copyright notices must remain intact in all applications, documentation,
// and source files.
//
//    Date     Who      Comments
// ==============================================================================================================
// 10/01/2006  Nissim   Created the code
//===============================================================================================================
//licHeader|


namespace Nistec.Channels
    {
        /// <summary>
        /// Represent a wrapper functionality for FileSystemWatcher,
        /// that Listens to the file system change notifications and raises events when a
        /// directory, or file in a directory, changes.
        /// </summary>
        public class IncomingFileWatcher
        {

        //private readonly MemoryCache _memCache;
        //private readonly CacheItemPolicy _cacheItemPolicy;
        //private const int CacheTimeMilliseconds = 1000;


       
        /// <summary>
        /// Default file name.
        /// </summary>
        public const string DefaultFileName = "mcSysWatcher.xml";
            /// <summary>
            /// Default file filter
            /// </summary>
            public const string DefaultFileFilter = "*.*";
            /// <summary>
            /// Get the system file path.
            /// </summary>
            public string SyncPath { get; private set; }
            /// <summary>
            /// Get the file name to watch.
            /// </summary>
            public string Filename { get; private set; }
            /// <summary>
            /// Get the file filter was specified by user.
            /// </summary>
            public string FileFilter { get; private set; }

            string FullPath()
            {
                return Path.Combine(SyncPath, Filename);
            }
            /// <summary>
            /// File Changed Action (Created, Deleted, Changed)
            /// </summary>
            public Action<string> FileChangedAction { get; set; }
            /// <summary>
            /// File Renamed Action (Renamed)
            /// </summary>
            public Action<RenamedEventArgs> FileRenamedAction { get; set; }

            /// <summary>
            /// File System Event Handler (Changed only)
            /// </summary>
            public event FileSystemEventHandler FileChanged;
        /// <summary>
        /// Occured when file has been changed (Created, Deleted, Changed).
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnFileChanged(FileSystemEventArgs e)
        {
            //if (FileChanged != null)
            //    FileChanged(this, e);
        }

        ///// <summary>
        ///// Initialize a new instance of SysFileWatcher
        ///// </summary>
        //public IncomingFileWatcher()
        //    : this(null, null)
        //{

        //}

        ///// <summary>
        ///// Initialize a new instance of SysFileWatcher
        ///// </summary>
        ///// <param name="path"></param>
        //public IncomingFileWatcher(string path)
        //        : this(path, null)
        //    {

        //    }

        /// <summary>
        /// Initialize a new instance of SysFileWatcher
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileFilter"></param>
        public IncomingFileWatcher(string path, string fileFilter, string docFilename)
            {
                //_memCache = MemoryCache.Default;

                //_cacheItemPolicy = new CacheItemPolicy()
                //{
                //    RemovedCallback = OnRemovedFromCache
                //};

                InitSysFileWatcher(path, fileFilter, docFilename);
            }

        ///// <summary>
        ///// Initialize a new instance of SysFileWatcher
        ///// </summary>
        ///// <param name="fpath"></param>
        ///// <param name="usefileNameAsFilter"></param>
        //public IncomingFileWatcher(string fpath, bool usefileNameAsFilter)
        //{
        //    string fileFilter = null;
        //    if (usefileNameAsFilter)
        //    {
        //        fileFilter = Path.GetFileName(fpath);
        //    }
        //    InitSysFileWatcher(fpath, fileFilter);
        //}

        private void InitSysFileWatcher(string path, string fileFilter, string docFilename)
        {


            //string fpath = CacheSettings.SyncConfigFile;
            if (string.IsNullOrEmpty(path))
            {
                SyncPath = SysNet.GetExecutingAssemblyPath();
                Filename = docFilename;// DefaultFileName;
            }
            else
            {
                Filename = docFilename;// Path.GetFileName(path);
                SyncPath = Path.GetDirectoryName(path);
            }

            if (string.IsNullOrEmpty(fileFilter))
            {
                FileFilter = DefaultFileFilter;
            }
            else
            {
                FileFilter = fileFilter;
            }


            //you can specify a file type or a specific filename as
            //the second parameter of FileSystemWatcher or *.* for all
            //type of files
            FileSystemWatcher WatchFile = new FileSystemWatcher(SyncPath, FileFilter);

            WatchFile.IncludeSubdirectories = false;
            WatchFile.NotifyFilter = NotifyFilters.LastWrite;

            //WatchFile.Created += new FileSystemEventHandler(WatchFile_CreatedDeleted);
            //WatchFile.Renamed += new RenamedEventHandler(WatchFile_Renamed);
            //WatchFile.Deleted += new FileSystemEventHandler(WatchFile_CreatedDeleted);
            WatchFile.Changed += new FileSystemEventHandler(WatchFile_Changed);

            WatchFile.EnableRaisingEvents = true;
        }

            DateTime lastTimeRead = DateTime.MinValue;
            string lastFileRead = "";
            WatcherChangeTypes lastChangeType = WatcherChangeTypes.All;

            /// <summary>
            /// Get the last time file was readed.
            /// </summary>
            public DateTime LastTimeRead
            {
                get { return lastTimeRead; }
            }
            /// <summary>
            /// Get the last file name was readed.
            /// </summary>
            public string LastFileRead
            {
                get { return lastFileRead; }
            }

        // Handle cache item expiring 
        //private void OnRemovedFromCache(CacheEntryRemovedArguments args)
        //{
        //    if (args.RemovedReason != CacheEntryRemovedReason.Expired) return;

        //    var e = (FileSystemEventArgs)args.CacheItem.Value;

        //    if (FileChanged != null)
        //        FileChanged(this, e);
        //    if (FileChangedAction != null)
        //        FileChangedAction(e);

        //    //Console.WriteLine($"Let's now respond to the event {e.ChangeType} on {e.FullPath}");
        //}

        //// Add file event to cache (won't add if already there so assured of only one occurance)
        //private void b_WatchFile_Changed(object source, FileSystemEventArgs e)
        //{
        //    _cacheItemPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddMilliseconds(CacheTimeMilliseconds);
        //    _memCache.AddOrGetEx   isting(e.Name, e, _cacheItemPolicy);
        //}

        internal void WatchFile_Changed(object sender, FileSystemEventArgs e)
        {
            if (FileChanged != null || FileChangedAction != null)
            {
                string filename = Path.Combine(e.FullPath,Filename);
                if (File.Exists(filename))
                {
                    string json= File.ReadAllText(filename);
                    File.Delete(filename);
                    if (FileChangedAction != null)
                        FileChangedAction(json);
                }
            }
            OnFileChanged(e);
        }

        //internal void WatchFile_Changed(object sender, FileSystemEventArgs e)
        //    {
        //        if (FileChanged != null || FileChangedAction != null)
        //        {
        //            if (Filename.ToLower() == e.Name.ToLower())
        //            {
        //                DateTime lastWriteTime = File.GetLastWriteTime(e.FullPath);

        //                if (lastWriteTime != lastTimeRead || lastFileRead != e.FullPath)
        //                {
        //                    lastTimeRead = lastWriteTime;
        //                    lastFileRead = e.FullPath;

        //                    if (FileChanged != null)
        //                        FileChanged(this, e);
        //                    if (FileChangedAction != null)
        //                        FileChangedAction(e);

        //                    Console.WriteLine(e.ToString());
        //                }
        //            }
        //        }
        //        OnFileChanged(e);
        //    }

        //internal void WatchFile_CreatedDeleted(object sender, FileSystemEventArgs e)
        //{
        //    if (FileChangedAction != null)
        //    {
        //        if (Filename.ToLower() == e.Name.ToLower())
        //        {
        //            DateTime lastWriteTime = File.GetLastWriteTime(e.FullPath);

        //            if ((lastWriteTime != lastTimeRead || lastFileRead != e.FullPath) && lastChangeType == e.ChangeType)
        //            {
        //                lastTimeRead = lastWriteTime;
        //                lastFileRead = e.FullPath;
        //                lastChangeType = e.ChangeType;

        //                FileChangedAction(e);

        //                Console.WriteLine(e.ToString());
        //            }
        //        }
        //    }
        //    OnFileChanged(e);
        //}

        //internal void WatchFile_Renamed(object sender, RenamedEventArgs e)
        //    {
        //        if (FileRenamedAction != null)
        //        {
        //            if (Filename.ToLower() == e.Name.ToLower())
        //            {
        //                DateTime lastWriteTime = File.GetLastWriteTime(e.FullPath);

        //                if (lastWriteTime != lastTimeRead || lastFileRead != e.FullPath)
        //                {
        //                    lastTimeRead = lastWriteTime;
        //                    lastFileRead = e.FullPath;

        //                    FileRenamedAction(e);

        //                    Console.WriteLine(e.ToString());

        //                    //Console.WriteLine("Change Type: {0}", e.ChangeType);
        //                    //Console.WriteLine("Full Path: {0}", e.FullPath);
        //                    //Console.WriteLine("Name: {0}", e.Name);
        //                    //Console.WriteLine("Old Full Path: {0}", e.OldFullPath);
        //                    //Console.WriteLine("Old Name: {0}", e.OldName);
        //                }
        //            }
        //        }
        //        OnFileChanged(e);
        //    }
        }
    }


