﻿using Controller.Agent.Model;
//licHeader
//===============================================================================================================
// System  : Nistec.Cache - Nistec.Cache Class Library
// Author  : Nissim Trujman  (nissim@nistec.net)
// Updated : 01/07/2015
// Note    : Copyright 2007-2015, Nissim Trujman, All rights reserved
// Compiler: Microsoft Visual C#
//
// This file contains a class that is part of cache core.
//
// This code is published under the Microsoft Public License (Ms-PL).  A copy of the license should be
// distributed with the code and can be found at the project website: http://nistec.net/license/nistec.cache-license.txt.  
// This notice, the author's name, and all copyright notices must remain intact in all applications, documentation,
// and source files.
//
//    Date     Who      Comments
// ==============================================================================================================
// 10/01/2006  Nissim   Created the code
//===============================================================================================================
//licHeader|
//using Nistec.Channels;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace Nistec.Channels
{
    /// <summary>
    /// Represent pipe client channel
    /// </summary>
    public class RemotePipeClient : Nistec.Channels.PipeClient<Message>, IDisposable
    {

        public static void SendOut(LogListItems items)
        {
            Message request = new Message(ChannelsSettings.cmd_request_logItems, Guid.NewGuid().ToString(), items, ChannelsSettings.ConnectTimeout);
            request.IsDuplex = false;
            using (RemotePipeClient client = new RemotePipeClient(ChannelsSettings.PipeName, false, false))
            {
                client.Execute(request, false);
            }
        }
        public static void SendOut(string log)
        {
            Message request = new Message(ChannelsSettings.cmd_request_logEvent, Guid.NewGuid().ToString(), log, ChannelsSettings.ConnectTimeout);
            request.IsDuplex = false;
            using (RemotePipeClient client = new RemotePipeClient(ChannelsSettings.PipeName, false, false))
            {
                client.Execute(request, false);
            }
        }

        #region static send methods with enableException


        /// <summary>
        /// Send Duplex message with return value.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="PipeName"></param>
        /// <param name="enableException"></param>
        /// <param name="IsAsync"></param>
        /// <returns></returns>
        public static object SendDuplex(Message request, string PipeName, bool enableException = false, bool IsAsync = false)
        {
            Type type = request.BodyType;
            request.IsDuplex = true;
            PipeDirection direction = request.IsDuplex ? PipeDirection.InOut : PipeDirection.Out;
            using (RemotePipeClient client = new RemotePipeClient(PipeName, true, IsAsync))
            {
                return client.Execute(request, type, enableException);
            }
        }
        /// <summary>
        /// Send Duplex message with return value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="PipeName"></param>
        /// <param name="enableException"></param>
        /// <param name="IsAsync"></param>
        /// <returns></returns>
        public static T SendDuplex<T>(Message request, string PipeName, bool enableException = false, bool IsAsync = false)
        {
            request.IsDuplex = true;
            using (RemotePipeClient client = new RemotePipeClient(PipeName, true, IsAsync))
            {
                return client.Execute<T>(request, enableException);
            }
        }
        /// <summary>
        /// Send Out message with no return value.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="PipeName"></param>
        /// <param name="enableException"></param>
        /// <param name="IsAsync"></param>
        public static void SendOut(Message request, string PipeName, bool enableException = false, bool IsAsync = false)
        {
            request.IsDuplex = false;
            using (RemotePipeClient client = new RemotePipeClient(PipeName, false, IsAsync))
            {
                client.Execute(request, enableException);
            }
        }
        /// <summary>
        /// Get message from server.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="PipeName"></param>
        /// <param name="enableException"></param>
        /// <param name="IsAsync"></param>
        public static void SendIn(Message request, string PipeName, bool enableException = false, bool IsAsync = false)
        {
            request.IsDuplex = false;
            using (RemotePipeClient client = new RemotePipeClient(PipeName, false, IsAsync))
            {
                client.PipeDirection = PipeDirection.In;
                client.Execute(request, enableException);
            }
        }
        #endregion

        #region ctor

        /// <summary>
        /// Constractor with arguments
        /// </summary>
        /// <param name="pipeName"></param>
        /// <param name="isDuplex"></param>
        /// <param name="isAsync"></param>
        public RemotePipeClient(string pipeName, bool isDuplex, bool isAsync)
            : base(pipeName, isDuplex, isAsync)
        {

        }

        /// <summary>
        /// Constractor with arguments
        /// </summary>
        /// <param name="pipeName"></param>
        /// <param name="inBufferSize"></param>
        /// <param name="outBufferSize"></param>
        /// <param name="isDuplex"></param>
        /// <param name="isAsync"></param>
        public RemotePipeClient(string pipeName, int inBufferSize, int outBufferSize, bool isDuplex, bool isAsync)
            : base(pipeName, inBufferSize, outBufferSize, isDuplex, isAsync)
        {

        }


        /// <summary>
        /// Constractor with extra parameters
        /// </summary>
        /// <param name="configHostName"></param>
        /// <param name="direction"></param>
        public RemotePipeClient(string configHostName, PipeDirection direction)
            : base(configHostName, direction)
        {

        }

        /// <summary>
        /// Constractor with extra parameters
        /// </summary>
        /// <param name="configHostName"></param>
        public RemotePipeClient(string configHostName)
            : base(configHostName, System.IO.Pipes.PipeDirection.InOut)
        {
        }

        /// <summary>
        /// Constractor with settings parameters
        /// </summary>
        /// <param name="settings"></param>
        public RemotePipeClient(PipeSettings settings)
            : base(settings)
        {

        }

        #endregion

        #region override
        /// <summary>
        /// Execute Message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        protected override object ExecuteMessage(Message message, Type type)
        {
            object response = null;

            if (PipeDirection != System.IO.Pipes.PipeDirection.In)
            {
                // Send a request from client to server
                message.EntityWrite(pipeClient, null);
            }

            if (PipeDirection == System.IO.Pipes.PipeDirection.Out)
            {
                return response;
            }

            // Receive a response from server.
            response = message.ReadResponse(pipeClient, type, Settings.InBufferSize);

            return response;
        }
        /// <summary>
        /// Execute Message
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override TResponse ExecuteMessage<TResponse>(Message message)
        {
            TResponse response = default(TResponse);

            if (PipeDirection != System.IO.Pipes.PipeDirection.In)
            {
                // Send a request from client to server
                message.EntityWrite(pipeClient, null);
            }

            if (PipeDirection == System.IO.Pipes.PipeDirection.Out)
            {
                return response;
            }

            // Receive a response from server.
            response = message.ReadResponse<TResponse>(pipeClient, Settings.InBufferSize);

            return response;
        }

        /// <summary>
        /// connect to the named pipe and execute request.
        /// </summary>
        public MessageAck Execute(Message message, bool enableException = false)
        {
            return Execute<MessageAck>(message, enableException);
        }



        #endregion

    }
}

