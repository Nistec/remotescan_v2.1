﻿//licHeader
//===============================================================================================================
// System  : Nistec.Cache - Nistec.Cache Class Library
// Author  : Nissim Trujman  (nissim@nistec.net)
// Updated : 01/07/2015
// Note    : Copyright 2007-2015, Nissim Trujman, All rights reserved
// Compiler: Microsoft Visual C#
//
// This file contains a class that is part of cache core.
//
// This code is published under the Microsoft Public License (Ms-PL).  A copy of the license should be
// distributed with the code and can be found at the project website: http://nistec.net/license/nistec.cache-license.txt.  
// This notice, the author's name, and all copyright notices must remain intact in all applications, documentation,
// and source files.
//
//    Date     Who      Comments
// ==============================================================================================================
// 10/01/2006  Nissim   Created the code
//===============================================================================================================
//licHeader|
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Pipes;
using Nistec.Channels;
//using Nistec.Caching.Remote;
using Nistec.IO;
using System.Threading.Tasks;
using RemoteController.Model;
using Nistec.Serialization;
using Nistec.Logging;
using RemoteController;
//using Nistec.Caching.Channels;
//using Nistec.Caching.Config;

namespace Nistec.Channels
{
    /// <summary>
    /// Represent a pipe server listner.
    /// </summary>
    public class RemotePipeServer : PipeServer<PipeMessage>//PipeServerCache
    {

        #region override
        /// <summary>
        /// OnStart
        /// </summary>
        protected override void OnStart()
        {
            base.OnStart();
            //AgentManager.Start();
        }
        /// <summary>
        /// OnStop
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();

            //AgentManager.Stop();
        }
        /// <summary>
        /// OnLoad
        /// </summary>
        protected override void OnLoad()
        {
            base.OnLoad();


        }
        #endregion

        #region ctor

        /// <summary>
        /// Constractor default
        /// </summary>
        public RemotePipeServer()
            : base(ChannelsSettings.GetPipeSettings())
        {
            //LoadRemoteCache();
        }

        /// <summary>
        /// Constractor with extra parameters
        /// </summary>
        /// <param name="name"></param>
        /// <param name="loadFromSettings"></param>
        public RemotePipeServer(string name, bool loadFromSettings)
            : base(name, loadFromSettings)
        {
            //LoadRemoteCache();
        }

        #endregion

        #region abstract methods
        /// <summary>
        /// Execute client request and return response as stream.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected override TransStream ExecRequset(PipeMessage message)
        {
            MessageState state = MessageState.None;
            string responseText="";
            try
            {
                if (message != null)
                {
                    switch (message.Command)
                    {
                        case ChannelsSettings.cmd_request_logEvent:
                            string logevent = message.DecodeBody<string>();
                            LogListItems.Instance.AppendLog(logevent);
                            responseText = "completed";
                            state = MessageState.Ok;
                            break;
                        case ChannelsSettings.cmd_request_scandoc:
                            {
                                var doc = message.DecodeBody<ScanDocument>();
                                ScanController scan = new ScanController(doc);
                                scan.ExecScan();
                                responseText = "completed";
                                state = MessageState.Ok;

                                Netlog.Info("Pipe ExecRequset: cmd_request_scandoc");
                            }
                            break;
                        //case ChannelsSettings.cmd_request_scan:
                        //    {
                        //        ScanModel scan = new ScanModel();
                        //        scan.ExecScan(null);
                        //        responseText = "completed";
                        //        state = MessageState.Ok;
                        //        Netlog.Info("Pipe ExecRequset: cmd_request_scan");
                        //    }
                        //    break;
                        default:
                            responseText = "none";
                            state = MessageState.NotSupportedError;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                state = MessageState.OperationError;
                responseText = ex.Message;
                Netlog.Exception("Pipe ExecRequset Error: ", ex);
            }

            return null;// MessageAck.DoResponse(state, responseText);
        }

        

        /// <summary>
        /// ReadRequest
        /// </summary>
        /// <param name="pipeServer"></param>
        /// <returns></returns>
        protected override PipeMessage ReadRequest(NamedPipeServerStream pipeServer)
        {
            return ReadRequest(pipeServer);//, InBufferSize);
        }
       
        /// <summary>
        /// Occured when new client connected to cache.
        /// </summary>
        protected override void OnClientConnected()
        {
            //Task.Factory.StartNew(() => AgentManager.Cache.PerformanceCounter.AddRequest());
        }

        ///// <summary>
        ///// Write Response
        ///// </summary>
        ///// <param name="pipeServer"></param>
        ///// <param name="bResponse"></param>
        ///// <param name="message"></param>
        //protected override void WriteResponse(NamedPipeServerStream pipeServer, NetStream bResponse, CacheMessage message)
        //{
        //    if (message.IsDuplex)
        //    {
        //        WriteResponse(pipeServer, bResponse);
        //    }
        //}
        #endregion

        #region Read/Write pipe
        
        /*
       
        internal object ReadResponse(NamedPipeClientStream stream, Type type, int InBufferSize = 8192)
        {


            using (AckStream ack = AckStream.Read(stream, type, InBufferSize))
            {
                if (ack.State > MessageState.Ok)
                {
                    throw new Exception(ack.Message);
                }
                return ack.Value;
            }
        }

        internal TResponse ReadResponse<TResponse>(NamedPipeClientStream stream, int InBufferSize = 8192)
        {

            using (AckStream ack = AckStream.Read(stream, typeof(TResponse), InBufferSize))
            {
                if (ack.State > MessageState.Ok)
                {
                    throw new Exception(ack.Message);
                }
                return ack.GetValue<TResponse>();
            }
        }
        */

        internal static PipeMessage ReadRequest(NamedPipeServerStream pipeServer, int InBufferSize = 8192)
        {
            PipeMessage message = new PipeMessage();
            message.EntityRead(pipeServer, null);
            return message;
        }

        //protected override void WriteResponse(NamedPipeServerStream pipeServer, NetStream bResponse)
        //{
        //    //base.WriteResponse(pipeServer, bResponse);
        //    if (bResponse == null)
        //    {
        //        return;
        //    }

        //    int cbResponse = bResponse.iLength;

        //    pipeServer.Write(bResponse.ToArray(), 0, cbResponse);

        //    pipeServer.Flush();
        //}
        
        #endregion
    }
}
