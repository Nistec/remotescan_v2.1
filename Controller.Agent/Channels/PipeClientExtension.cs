﻿using RemoteController.Model;
using Nistec.Channels;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace Nistec.Channels
{
    public static class PipeClientExtension
    {
 
        public static void SendOut(LogListItems items)
        {
            PipeMessage request = new PipeMessage(ChannelsSettings.cmd_request_logItems, Guid.NewGuid().ToString(), items, ChannelsSettings.ConnectTimeout);
            request.IsDuplex = false;
            using (PipeClient client = new PipeClient(ChannelsSettings.PipeName, false, PipeOptions.None))
            {
                client.Execute(request, false);
            }
        }
        public static void SendOut(string log)
        {
            PipeMessage request = new PipeMessage(ChannelsSettings.cmd_request_logEvent, Guid.NewGuid().ToString(), log, ChannelsSettings.ConnectTimeout);
            request.IsDuplex = false;
            using (PipeClient client = new PipeClient(ChannelsSettings.PipeName, false, PipeOptions.None))
            {
                client.Execute(request, false);
            }
        }
        public static void SendOut(ScanDocument doc)
        {
            //string command = doc == null ? ChannelsSettings.cmd_request_scan : ChannelsSettings.cmd_request_scandoc;

            string command = ChannelsSettings.cmd_request_scandoc;

            PipeMessage request = new PipeMessage(command, Guid.NewGuid().ToString(), doc, ChannelsSettings.ConnectTimeout);
            request.IsDuplex = false;
            using (PipeClient client = new PipeClient(ChannelsSettings.PipeName, false, PipeOptions.None))
            {
                client.Execute(request, false);
            }
        }
        public static void SendOutScan()
        {
            PipeMessage request = new PipeMessage(ChannelsSettings.cmd_request_scandoc, Guid.NewGuid().ToString(), ChannelsSettings.cmd_request_scandoc, ChannelsSettings.ConnectTimeout);
            request.IsDuplex = false;
            using (PipeClient client = new PipeClient(ChannelsSettings.PipeName, false, PipeOptions.None))
            {
                client.Execute(request, false);
            }
        }
        public static object SendDuplex(string log)
        {
            PipeMessage request = new PipeMessage(ChannelsSettings.cmd_request_logEvent, Guid.NewGuid().ToString(), log, ChannelsSettings.ConnectTimeout);
            //Type type = request.BodyType;
            request.IsDuplex = true;
            PipeDirection direction = request.IsDuplex ? PipeDirection.InOut : PipeDirection.Out;
            using (PipeClient client = new PipeClient(ChannelsSettings.PipeName, true, PipeOptions.None))
            {
                return client.Execute(request, false);
            }
        }
        public static object SendDuplex(PipeMessage request, string PipeName, bool enableException = false, bool IsAsync = false)
        {
            //Type type = request.BodyType;
            request.IsDuplex = true;
            PipeOptions options = IsAsync ? PipeOptions.Asynchronous : PipeOptions.None;
            PipeDirection direction = request.IsDuplex ? PipeDirection.InOut : PipeDirection.Out;
            using (PipeClient client = new PipeClient(PipeName, true, options))
            {
                return client.Execute(request, enableException);
            }
        }
    }
}
