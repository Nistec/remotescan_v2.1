﻿//licHeader
//===============================================================================================================
// System  : Nistec.Cache - Nistec.Cache Class Library
// Author  : Nissim Trujman  (nissim@nistec.net)
// Updated : 01/07/2015
// Note    : Copyright 2007-2015, Nissim Trujman, All rights reserved
// Compiler: Microsoft Visual C#
//
// This file contains a class that is part of cache core.
//
// This code is published under the Microsoft Public License (Ms-PL).  A copy of the license should be
// distributed with the code and can be found at the project website: http://nistec.net/license/nistec.cache-license.txt.  
// This notice, the author's name, and all copyright notices must remain intact in all applications, documentation,
// and source files.
//
//    Date     Who      Comments
// ==============================================================================================================
// 10/01/2006  Nissim   Created the code
//===============================================================================================================
//licHeader|
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nistec.Serialization;
using Nistec.Channels;
using System.IO.Pipes;

namespace Nistec.Channels
{
    /// <summary>
    /// Represent Cache Defaults Settings
    /// </summary>
    public class ChannelsSettings
    {

        public static PipeSettings GetPipeSettings(PipeDirection direction = PipeDirection.InOut)
        {
            return new PipeSettings()
            {
                ConnectTimeout = ChannelsSettings.ConnectTimeout,
                ReceiveBufferSize = ChannelsSettings.InBufferSize,
                MaxAllowedServerInstances = System.IO.Pipes.NamedPipeServerStream.MaxAllowedServerInstances,
                MaxServerConnections = ChannelsSettings.MaxServerConnections,
                SendBufferSize = ChannelsSettings.OutBufferSize,
                PipeDirection = direction,
                PipeName = ChannelsSettings.PipeName,
                PipeOptions = PipeOptions.None,
                VerifyPipe = ChannelsSettings.PipeName
            };
        }

        /// <summary>
        /// Get Default Formatter
        /// </summary>
        public static Formatters DefaultFormatter { get { return Formatters.BinarySerializer; } }
        /// <summary>
        /// Default protocol
        /// </summary>
        public static NetProtocol DefaultProtocol = NetProtocol.Pipe;

        #region pipe

        public const string PipeName = "remote_scan";
        public const int ConnectTimeout = 5000;
        public const int InBufferSize = 8192;
        public const int OutBufferSize = 8192;

        public const int MaxAllowedServerInstances = System.IO.Pipes.NamedPipeServerStream.MaxAllowedServerInstances;
        public const int MaxServerConnections = 1;

        /// <summary>
        /// Default HostName
        /// </summary>
        public const string DefaultHostName = "remote_scan";
       
        /// <summary>
        /// Default Manager HostName
        /// </summary>
        public const string ManagerHostName = "remote_scan_manager";

        #endregion

        #region pipe command

        public const string cmd_request_logEvent = "cmd_request_logevent";
        public const string cmd_request_logItems = "cmd_request_logitems";
        //public const string cmd_request_scan = "cmd_request_scan";
        public const string cmd_request_scandoc = "cmd_request_scandoc";

        #endregion

        /// <summary>
        /// Get Default Expiration in minutes
        /// </summary>
        public static int DefaultExpiration { get; internal set; }
        /// <summary>
        /// Get if Enable Logging
        /// </summary>
        public static bool EnableLog { get; internal set; }

        public static string FileWatcherName = "LogListItems.txt";

        


    }

}
