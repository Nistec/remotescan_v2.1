﻿using Nistec.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32;

namespace RemoteController.Model
{
    public class WinProc
    {

        [DllImport("user32.dll")]
        private static extern
            bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern
            bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern
            bool IsIconic(IntPtr hWnd);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;


        [DllImport("user32.dll")]
        public static extern int SetWindowText(IntPtr hWnd, string text);
        [DllImport("user32.dll")]
        public static extern string GetWindowText(IntPtr hWnd);

        public static void SetCurrentTitle(string title) {

            var proc = System.Diagnostics.Process.GetCurrentProcess();
            SetWindowText(proc.MainWindowHandle, title);
        }

        public static string GetCurrentTitle(Process proc)
        {

            //var proc = System.Diagnostics.Process.GetCurrentProcess();
            return GetWindowText(proc.MainWindowHandle);
        }


        //private void StartMyNotepad()
        //{
        //    Process p = Process.Start("notepad.exe");
        //    Thread.Sleep(100);  // <-- ugly hack
        //    SetWindowText(p.MainWindowHandle, "My Notepad");

        //    IntPtr handle = p.MainWindowHandle;
        //    SetWindowText(handle, "This is my new title");
        //}


        static int GetCmdShow(ProcessWindowStyle windowStyle )
        {
            switch(windowStyle)
            {
                case ProcessWindowStyle.Hidden:
                    return SW_HIDE;
                case ProcessWindowStyle.Maximized:
                    return SW_SHOWMAXIMIZED;
                case ProcessWindowStyle.Minimized:
                    return SW_SHOWMINIMIZED;
                case ProcessWindowStyle.Normal:
                    return SW_SHOWNORMAL;
                default:
                    return SW_RESTORE;

            }
        }

        public static int KillProcess(string procName)
        {
            int traminated = 0;
            Process[] runningProcesses = Process.GetProcessesByName(procName);
            foreach (var proc in runningProcesses)
            {
                string name = proc.ProcessName;
                Console.WriteLine("KillProcess " + name);
                try
                {
                    proc.Kill();
                    traminated++;
                }
                catch(Exception ex)
                {
                    //Console.WriteLine("KillProcess " + name + ", Error: " + ex.Message);
                    LogListItems.Instance.AppendLog("KillProcess " + name + ", Error: " + ex.Message);
                }
            }

            return traminated;
        }
        public static int TerminateProcess(string procName)
        {
            int traminated = 0;
            Process[] runningProcesses = Process.GetProcessesByName(procName);
            foreach (var proc in runningProcesses)
            {
                string name= proc.ProcessName;
                Console.WriteLine("TerminateProcess "+ name);

                //IntPtr hWnd = proc.MainWindowHandle;

                // Close process by sending a close message to its main window.
                bool isclosed= proc.CloseMainWindow();
                // Free resources associated with process.
                //proc.Close();

                proc.Kill();

                if(isclosed)
                traminated++;
            }
            return traminated;
        }

        public static void TerminateProcessExceptCurrentSession(string filename)
        {
            var procName = Path.GetFileNameWithoutExtension(filename);

            Process[] runningProcesses = Process.GetProcessesByName(procName);

            var currentSessionID = Process.GetCurrentProcess().SessionId;

            Process process =
                runningProcesses.Where(p => p.SessionId != currentSessionID).FirstOrDefault();

            if (process != null)
            {

                string name = process.ProcessName;
                Console.WriteLine("TerminateProcess " + name);

                //IntPtr hWnd = proc.MainWindowHandle;

                // Close process by sending a close message to its main window.
                bool isclosed = process.CloseMainWindow();
                // Free resources associated with process.
                process.Close();

                process.Kill();
            }
        }

        public static void TerminateProcessExceptCurrentSession(string filename, string id)
        {
            var procName = Path.GetFileNameWithoutExtension(filename);

            Process[] runningProcesses = Process.GetProcessesByName(procName);

            //var currentTitle = GetCurrentTitle();

            Process process =
                runningProcesses.Where(p => GetCurrentTitle(p) != id).FirstOrDefault();

            if (process != null)
            {

                string name = process.ProcessName;
                Console.WriteLine("TerminateProcess " + name);

                //IntPtr hWnd = proc.MainWindowHandle;

                // Close process by sending a close message to its main window.
                bool isclosed = process.CloseMainWindow();
                // Free resources associated with process.
                process.Close();

                process.Kill();
            }
        }

        public static void ShowSessionApp(string filename, string args, ProcessWindowStyle windowStyle)
        {
            var procName = Path.GetFileNameWithoutExtension(filename);

            Process[] runningProcesses = Process.GetProcessesByName(procName);
            
            var currentSessionID = Process.GetCurrentProcess().SessionId;

            Process process =
                runningProcesses.Where(p => p.SessionId == currentSessionID).FirstOrDefault();

            if (process != null)
            {
                // get the window handle
                IntPtr hWnd = process.MainWindowHandle;
                ActivateApp(hWnd, GetCmdShow(windowStyle));
            }
            else
            {
                RunApp(filename, args, windowStyle, true);
            }
        }

        public static void ShowApp(string filename, string args)
        {
            var procName = Path.GetFileNameWithoutExtension(filename);

            Process[] processes = Process.GetProcessesByName(procName);
            if (processes.Length > 0)
            {
                // get the window handle
                IntPtr hWnd = processes[0].MainWindowHandle;
                ActivateApp(hWnd, SW_RESTORE);
            }
            else
            {
                RunApp(filename, args, ProcessWindowStyle.Normal,true);
            }
        }


        public static void ActivateApp(IntPtr hWnd, int cmdShow)
        {
            try
            {
                //if (hWnd == IntPtr.Zero)
                //    return;

                // if iconic, restore the window
                if (IsIconic(hWnd))
                {
                    ShowWindowAsync(hWnd, cmdShow);//SW_RESTORE);
                }
                // bring it to the foreground
                SetForegroundWindow(hWnd);
                ShowWindow(hWnd, cmdShow);
            }
            catch(Exception ex)
            {
                LogListItems.Instance.AppendLog("Error: ActivateApp " + ex.Message);
            }
        }

        public static void RunApp(string filename, string args, ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal, bool setToFront = false)
        {

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = filename;
            startInfo.WindowStyle = windowStyle;
            if (args != null)
                startInfo.Arguments = args;

            using (Process exeProcess = Process.Start(startInfo))
            {
                if (setToFront)
                    ActivateApp(exeProcess.MainWindowHandle, GetCmdShow(windowStyle));
                
                //exeProcess.WaitForExit();
            }
        }

        public static int RunAppAs(string appName, string filename, string args, string username,string password, bool isSilentMode = true)
        {

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = isSilentMode;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.LoadUserProfile = true;
            if (username != null)
            {
                startInfo.Verb = "runas";
                startInfo.UserName = username;
                //startInfo.Password = "XXXX";
            }
            startInfo.FileName = filename;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            if (args != null)
                startInfo.Arguments = args;

            int exitCode = 0;

            using (Process exeProcess = Process.Start(startInfo))
            {
                //var miliseconds=TimeSpan.FromSeconds(30).TotalMilliseconds;

                try
                {
                    string stdout = exeProcess.StandardOutput.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(stdout))
                    {
                        exitCode = 1;
                        LogListItems.Instance.AppendLog("Warn: " + appName + " message " + stdout);
                    }

                    string stderr = exeProcess.StandardError.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(stderr))
                    {
                        exitCode = -1;
                        LogListItems.Instance.AppendLog("Error: " + appName + " error " + stderr);
                    }

                    if (exitCode == 0)
                        exitCode = exeProcess.ExitCode;

                    //if (waitForExitMilliseconds == 0)
                    //    exeProcess.WaitForExit();
                    //else if (waitForExitMilliseconds > 0)
                    //    exeProcess.WaitForExit(waitForExitMilliseconds);//30seconds

                }
                catch (Exception ex)
                {
                    LogListItems.Instance.AppendLog("Error: RunAppAs " + ex.Message);
                }

                return exitCode;
            }
        }

        public static int RunAppSilent(string appName,string filename, string args, bool isSilentMode = true, int waitForExitMilliseconds = 0)
        {

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = isSilentMode;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;

            startInfo.FileName = filename;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            if (args != null)
                startInfo.Arguments = args;
            
            int exitCode = 0;

            using (Process exeProcess = Process.Start(startInfo))
            {
                //var miliseconds=TimeSpan.FromSeconds(30).TotalMilliseconds;

                try
                {
                    string stdout = exeProcess.StandardOutput.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(stdout))
                    {
                        exitCode = 1;
                        LogListItems.Instance.AppendLog("Warn: " + appName + " message " + stdout);
                    }

                    string stderr = exeProcess.StandardError.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(stderr))
                    {
                        exitCode = -1;
                        LogListItems.Instance.AppendLog("Error: " + appName + " error " + stderr);
                    }

                    if (exitCode == 0)
                        exitCode = exeProcess.ExitCode;

                    if (waitForExitMilliseconds == 0)
                        exeProcess.WaitForExit();
                    else if (waitForExitMilliseconds > 0)
                        exeProcess.WaitForExit(waitForExitMilliseconds);//30seconds

                }
                catch(Exception ex)
                {
                    LogListItems.Instance.AppendLog("Error: RunAppSilent " + ex.Message);
                }

                return exitCode;
            }
        }

       

        //mode: 1: startup , -1 :delete
        public static void SetStartup(int mode)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string AppName = ScanSettings.AppName;

            if (mode == 1)
                rk.SetValue(AppName, location);
            else if (mode == -1)
                rk.DeleteValue(AppName, false);
        }

    }
}
