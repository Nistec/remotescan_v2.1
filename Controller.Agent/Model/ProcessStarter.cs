using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using Nistec.Logging;

namespace RemoteController.Model
{
    public class ProcessStarter : IDisposable
    {
        #region Import Section

        private static uint STANDARD_RIGHTS_REQUIRED = 0x000F0000;
        private static uint STANDARD_RIGHTS_READ = 0x00020000;
        private static uint TOKEN_ASSIGN_PRIMARY = 0x0001;
        private static uint TOKEN_DUPLICATE = 0x0002;
        private static uint TOKEN_IMPERSONATE = 0x0004;
        private static uint TOKEN_QUERY = 0x0008;
        private static uint TOKEN_QUERY_SOURCE = 0x0010;
        private static uint TOKEN_ADJUST_PRIVILEGES = 0x0020;
        private static uint TOKEN_ADJUST_GROUPS = 0x0040;
        private static uint TOKEN_ADJUST_DEFAULT = 0x0080;
        private static uint TOKEN_ADJUST_SESSIONID = 0x0100;
        private static uint TOKEN_READ = (STANDARD_RIGHTS_READ | TOKEN_QUERY);
        private static uint TOKEN_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE | TOKEN_IMPERSONATE | TOKEN_QUERY | TOKEN_QUERY_SOURCE | TOKEN_ADJUST_PRIVILEGES | TOKEN_ADJUST_GROUPS | TOKEN_ADJUST_DEFAULT | TOKEN_ADJUST_SESSIONID);

        private const uint NORMAL_PRIORITY_CLASS = 0x0020;

        private const uint CREATE_UNICODE_ENVIRONMENT = 0x00000400;


        private const uint MAX_PATH = 260;

        private const uint CREATE_NO_WINDOW = 0x08000000;
        private const uint CREATE_NEW_CONSOLE = 0x00000010;

        private const uint INFINITE = 0xFFFFFFFF;


        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;


        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        public enum SECURITY_IMPERSONATION_LEVEL
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }

        public enum TOKEN_TYPE
        {
            TokenPrimary = 1,
            TokenImpersonation
        }

        public enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public uint dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WTS_SESSION_INFO
        {
            public Int32 SessionID;

            [MarshalAs(UnmanagedType.LPStr)]
            public String pWinStationName;

            public WTS_CONNECTSTATE_CLASS State;
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern uint WTSGetActiveConsoleSessionId();

        [DllImport("wtsapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool WTSQueryUserToken(int sessionId, out IntPtr tokenHandle);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool DuplicateTokenEx(IntPtr existingToken, uint desiredAccess, IntPtr tokenAttributes, SECURITY_IMPERSONATION_LEVEL impersonationLevel, TOKEN_TYPE tokenType, out IntPtr newToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool CreateProcessAsUser(IntPtr token, string applicationName, string commandLine, ref SECURITY_ATTRIBUTES processAttributes, ref SECURITY_ATTRIBUTES threadAttributes, bool inheritHandles, uint creationFlags, IntPtr environment, string currentDirectory, ref STARTUPINFO startupInfo, out PROCESS_INFORMATION processInformation);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool CloseHandle(IntPtr handle);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetLastError();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int WaitForSingleObject(IntPtr token, uint timeInterval);

        [DllImport("wtsapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int WTSEnumerateSessions(System.IntPtr hServer, int Reserved, int Version, ref System.IntPtr ppSessionInfo, ref int pCount);

        [DllImport("userenv.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool CreateEnvironmentBlock(out IntPtr lpEnvironment, IntPtr hToken, bool bInherit);

        [DllImport("wtsapi32.dll", ExactSpelling = true, SetLastError = false)]
        public static extern void WTSFreeMemory(IntPtr memory);

        [DllImport("userenv.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool DestroyEnvironmentBlock(IntPtr lpEnvironment);


        //==============================================================

        private static uint STARTF_USESHOWWINDOW = 0x00000001;
        private static uint STARTF_USESTDHANDLES = 0x00000100;
        private static uint STARTF_FORCEONFEEDBACK = 0x00000040;
        //private static uint NORMAL_PRIORITY_CLASS = 0x00000020;
        private static uint CREATE_BREAKAWAY_FROM_JOB = 0x01000000;
        //private static uint CREATE_NO_WINDOW = 0x08000000;
        //private static uint CREATE_UNICODE_ENVIRONMENT = 0x00000400;
        //private static short SW_SHOW = 5;
        //private static short SW_HIDE = 0;
        private const int STD_OUTPUT_HANDLE = -11;
        private const int HANDLE_FLAG_INHERIT = 1;
        private static uint GENERIC_READ = 0x80000000;
        private static uint FILE_ATTRIBUTE_READONLY = 0x00000001;
        private static uint FILE_ATTRIBUTE_NORMAL = 0x00000080;
        private const int OPEN_EXISTING = 3;
        //private static uint CREATE_NEW_CONSOLE = 0x00000010;
        private static uint STILL_ACTIVE = 0x00000103;


        [DllImport("kernel32.dll")]
        static extern bool CreatePipe(out IntPtr phReadPipe, out IntPtr phWritePipe, IntPtr lpPipeAttributes, uint nSize);
        
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern unsafe bool ReadFile(
        IntPtr hfile,
        void* pBuffer,
        int NumberOfBytesToRead,
        int* pNumberOfBytesRead,
        int pOverlapped
        );

        [DllImport("kernel32.dll")]
        static extern IntPtr GetStdHandle(int StdHandle);
        [DllImport("kernel32.dll")]
        static extern bool SetStdHandle(int nStdHandle, IntPtr hHandle);
        [DllImport("kernel32.dll")]
        static extern bool SetHandleInformation(IntPtr hObject, int dwMask, uint dwFlags);
        [DllImport("kernel32", SetLastError = true)]
        static extern IntPtr CreateFile(string filename,
        uint desiredAccess,
        uint shareMode,
        IntPtr attributes,
        uint creationDisposition,
        uint flagsAndAttributes,
        IntPtr templateFile);

        public static unsafe int Read(byte[] buffer, int index, int count, IntPtr hStdOut)
        {
            int n = 0;
            fixed (byte* p = buffer)
            {
                if (!ReadFile(hStdOut, p + index, count, &n, 0))
                    return 0;
            }
            return n;
        }

        //==============================================================

        #endregion

        public ProcessStarter()
        {

        }

        public ProcessStarter(string processName, string fullExeName)
        {
            _processName = processName;
            _processPath = fullExeName;
        }
        public ProcessStarter(string processName, string fullExeName, string arguments)
        {
            _processName = processName;
            _processPath = fullExeName;
            _arguments = arguments;
        }

        public static IntPtr GetCurrentUserToken()
        {
            IntPtr currentToken = IntPtr.Zero;
            IntPtr primaryToken = IntPtr.Zero;
            IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;

            int dwSessionId = 0;
            IntPtr hUserToken = IntPtr.Zero;
            IntPtr hTokenDup = IntPtr.Zero;

            IntPtr pSessionInfo = IntPtr.Zero;
            int dwCount = 0;

            WTSEnumerateSessions(WTS_CURRENT_SERVER_HANDLE, 0, 1, ref pSessionInfo, ref dwCount);

            Int32 dataSize = Marshal.SizeOf(typeof(WTS_SESSION_INFO));

            //Int64 current = (Int64)pSessionInfo;
            System.IntPtr current = (System.IntPtr)pSessionInfo;
            for (int i = 0; i < dwCount; i++)
            {
                WTS_SESSION_INFO si = (WTS_SESSION_INFO)Marshal.PtrToStructure(current, typeof(WTS_SESSION_INFO));
                if (WTS_CONNECTSTATE_CLASS.WTSActive == si.State)
                {
                    dwSessionId = si.SessionID;
                    break;
                }

                current += dataSize;
            }

            WTSFreeMemory(pSessionInfo);

            bool bRet = WTSQueryUserToken(dwSessionId, out currentToken);
            if (bRet == false)
            {
                return IntPtr.Zero;
            }

            bRet = DuplicateTokenEx(currentToken, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, IntPtr.Zero, SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation, TOKEN_TYPE.TokenPrimary, out primaryToken);
            if (bRet == false)
            {
                return IntPtr.Zero;
            }

            return primaryToken;
        }


        public int RunNaps(uint waitForExit)
        {
            IntPtr primaryToken = GetCurrentUserToken();
            if (primaryToken == IntPtr.Zero)
            {
                Netlog.WarnFormat("Invalid Token for :{0}", _processName);
                return -1;
            }
            Netlog.DebugFormat("CurrentUserToken: {0} for :{1}", primaryToken.ToString(), _processName);

            STARTUPINFO StartupInfo = new STARTUPINFO();
            _processInfo = new PROCESS_INFORMATION();
            StartupInfo.cb = Marshal.SizeOf(StartupInfo);

            SECURITY_ATTRIBUTES Security1 = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES Security2 = new SECURITY_ATTRIBUTES();


            //============================================================================

            IntPtr hReadIn, hReadOut, hWriteIn, hWriteOut;
            IntPtr hStdout;
            IntPtr hInputFile;

            bool bret;

            IntPtr mypointer = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(STARTUPINFO)));
            Marshal.StructureToPtr(Security1, mypointer, true);
            bret = CreatePipe(out hReadOut, out hWriteOut, mypointer, 0);
            //ensure the read handle to pipe for stdout is not inherited
            SetHandleInformation(hReadOut, HANDLE_FLAG_INHERIT, 0);
            //SetHandleInformation(hWriteOut, HANDLE_FLAG_INHERIT, 0);
            ////Create pipe for the child process's STDIN
            //bret = CreatePipe(out hReadIn, out hWriteIn, IntPtr.Zero, 1024);
            bret = CreatePipe(out hReadIn, out hWriteIn, mypointer, 1024);

            ////ensure the write handle to the pipe for stdin is not inherited

            SetHandleInformation(hWriteIn, HANDLE_FLAG_INHERIT, 0);

            //StartupInfo = new STARTUPINFO();
            //si.cb = (uint)System.Runtime.InteropServices.Marshal.SizeOf(si);
            StartupInfo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
            StartupInfo.wShowWindow = SW_HIDE; // SW_HIDE; //SW_SHOW
            StartupInfo.hStdOutput = hWriteOut;
            StartupInfo.hStdError = hWriteOut;
            StartupInfo.hStdInput = hWriteIn;

            //============================================================================

            
            string command = "\"" + _processPath + "\"";
            if ((_arguments != null) && (_arguments.Length != 0))
            {
                command += " " + _arguments;
            }

            IntPtr lpEnvironment = IntPtr.Zero;
            bool resultEnv = CreateEnvironmentBlock(out lpEnvironment, primaryToken, false);
            if (resultEnv != true)
            {
                int nError = GetLastError();
                Netlog.WarnFormat("CreateEnvironmentBlock Error: {0} for :{1}", nError, _processName);
            }
            Netlog.DebugFormat("CreateEnvironmentBlock result: {0} for :{1}", resultEnv, _processName);

            CreateProcessAsUser(primaryToken, null, command, ref Security1, ref Security2, false, CREATE_NO_WINDOW | NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT, lpEnvironment, null, ref StartupInfo, out _processInfo);

            //IntPtr hStdOutput = StartupInfo.hStdOutput;

            //Process process = Process.GetProcessById(_processInfo.dwProcessId);

            //string stdout = process.StandardOutput.ReadToEnd();
            //if (!string.IsNullOrWhiteSpace(stdout))
            //{
            //   // exitCode = 1;
            //    LogListItems.Instance.AppendLog("Warn: " + _processName + " message " + stdout);
            //}

            //string stderr = process.StandardError.ReadToEnd();
            //if (!string.IsNullOrWhiteSpace(stderr))
            //{
            //    //exitCode = -1;
            //    LogListItems.Instance.AppendLog("Error: " + _processName + " error " + stderr);
            //}

            var ret = WaitForSingleObject(_processInfo.hProcess, waitForExit);//INFINITE);

            
            //Console.Write("WaitForSingleObject returned " + ret);
            //ret==258 (0x102) - not signalled, ret==0 ok!
            byte[] buffer = new byte[2048];

            ret = Read(buffer, 0, buffer.Length, hReadOut);
            String outs = Encoding.ASCII.GetString(buffer, 0, ret);

            Netlog.DebugFormat("Run Naps result: {0} for :{1}", outs, _processName);

            DestroyEnvironmentBlock(lpEnvironment);
            CloseHandle(primaryToken);

            //int errorcode = GetLastError();
            return ret;// errorcode;
        }

        public void RunEnterpriseScan()
        {

            IntPtr primaryToken = GetCurrentUserToken();
            if (primaryToken == IntPtr.Zero)
            {
                Netlog.WarnFormat("Invalid Token for :{0}", _processName);
                return;
            }
            Netlog.DebugFormat("CurrentUserToken: {0} for :{1}", primaryToken.ToString(), _processName);

            STARTUPINFO StartupInfo = new STARTUPINFO();
            _processInfo = new PROCESS_INFORMATION();
            StartupInfo.cb = Marshal.SizeOf(StartupInfo);
            StartupInfo.wShowWindow = SW_SHOWMAXIMIZED;

            SECURITY_ATTRIBUTES Security1 = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES Security2 = new SECURITY_ATTRIBUTES();

            string command = "\"" + _processPath + "\"";
            if ((_arguments != null) && (_arguments.Length != 0))
            {
                command += " " + _arguments;
            }

            IntPtr lpEnvironment = IntPtr.Zero;
            bool resultEnv = CreateEnvironmentBlock(out lpEnvironment, primaryToken, false);
            if (resultEnv != true)
            {
                int nError = GetLastError();
                Netlog.WarnFormat("CreateEnvironmentBlock Error: {0} for :{1}", nError, _processName);
            }
            Netlog.DebugFormat("CreateEnvironmentBlock result: {0} for :{1}", resultEnv, _processName);

            CreateProcessAsUser(primaryToken, null, command, ref Security1, ref Security2, false, CREATE_NEW_CONSOLE|NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT, lpEnvironment, null, ref StartupInfo, out _processInfo);

            WinProc.ActivateApp(_processInfo.hProcess, (int)ProcessWindowStyle.Maximized);

            DestroyEnvironmentBlock(lpEnvironment);
            CloseHandle(primaryToken);
        }


        public void Run(Action<IntPtr> action)//, ProcessWindowStyle windowStyle= ProcessWindowStyle.Normal)
        {

            IntPtr primaryToken = GetCurrentUserToken();
            if (primaryToken == IntPtr.Zero)
            {
                Netlog.WarnFormat("Invalid Token for :{0}", _processName);
                return;
            }
            Netlog.DebugFormat("CurrentUserToken: {0} for :{1}", primaryToken.ToString(), _processName);

            STARTUPINFO StartupInfo = new STARTUPINFO();
            _processInfo = new PROCESS_INFORMATION();
            StartupInfo.cb = Marshal.SizeOf(StartupInfo);
            //if (windowStyle != ProcessWindowStyle.Normal)
            //StartupInfo.wShowWindow = (short)windowStyle;

            SECURITY_ATTRIBUTES Security1 = new SECURITY_ATTRIBUTES();
            SECURITY_ATTRIBUTES Security2 = new SECURITY_ATTRIBUTES();

            string command = "\"" + _processPath + "\"";
            if ((_arguments != null) && (_arguments.Length != 0))
            {
                command += " " + _arguments;
            }

            IntPtr lpEnvironment = IntPtr.Zero;
            bool resultEnv = CreateEnvironmentBlock(out lpEnvironment, primaryToken, false);
            if (resultEnv != true)
            {
                int nError = GetLastError();
                Netlog.WarnFormat("CreateEnvironmentBlock Error: {0} for :{1}", nError, _processName);
            }
            Netlog.DebugFormat("CreateEnvironmentBlock result: {0} for :{1}", resultEnv, _processName);

            CreateProcessAsUser(primaryToken, null, command, ref Security1, ref Security2, false, CREATE_NO_WINDOW | NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT, lpEnvironment, null, ref StartupInfo, out _processInfo);

            //IntPtr hStdOutput = StartupInfo.hStdOutput;

            //Process process = Process.GetProcessById(_processInfo.dwProcessId);

            //string stdout = process.StandardOutput.ReadToEnd();
            //if (!string.IsNullOrWhiteSpace(stdout))
            //{
            //   // exitCode = 1;
            //    LogListItems.Instance.AppendLog("Warn: " + _processName + " message " + stdout);
            //}

            //string stderr = process.StandardError.ReadToEnd();
            //if (!string.IsNullOrWhiteSpace(stderr))
            //{
            //    //exitCode = -1;
            //    LogListItems.Instance.AppendLog("Error: " + _processName + " error " + stderr);
            //}


            if (action != null)
            {
                Netlog.DebugFormat("ActivateApp for :{0}",  _processName);
                //action(primaryToken);
                WinProc.ActivateApp(_processInfo.hProcess, (int)ProcessWindowStyle.Maximized);
            }

            DestroyEnvironmentBlock(lpEnvironment);
            CloseHandle(primaryToken);
        }

        public void Stop()
        {
            Process[] processes = Process.GetProcesses();
            foreach (Process current in processes)
            {
                if (current.ProcessName == _processName)
                {
                    current.Kill();
                }
            }
        }

       
        public int WaitForExit()
        {
            WaitForSingleObject(_processInfo.hProcess, INFINITE);
            int errorcode = GetLastError();
            return errorcode;
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        private string _processPath = string.Empty;
        private string _processName = string.Empty;
        private string _arguments = string.Empty;
        private PROCESS_INFORMATION _processInfo;

        public string ProcessPath
        {
            get
            {
                return _processPath;
            }
            set
            {
                _processPath = value;
            }
        }

        public string ProcessName
        {
            get
            {
                return _processName;
            }
            set
            {
                _processName = value;
            }
        }

        public string Arguments
        {
            get
            {
                return _arguments;
            }
            set
            {
                _arguments = value;
            }
        }
    }
}
