﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteController.Model
{
    public class ResultModel
    {
        public string Status { get; set; }
        public string Message { get; set; }

    }
}
