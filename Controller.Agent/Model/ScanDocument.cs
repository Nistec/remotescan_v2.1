﻿using Newtonsoft.Json;
using Nistec;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace RemoteController.Model
{
    /*
             {
                    "ScanParms": {
                        "ScanType": "naps",
                        "EnableEntScan": "true"
                    },
                    "ApplicationParams":
                        {
                            "SystemCode": "123",
                            "SystemName": "abc",
                            "InstituteCode": "123",
                            "IntituteName": "abc",
                            "DepartmentCode": "123",
                            "DepartmentName": "abc",
                            "UserName": "user",
                            "PatientIDType": "1",
                            "PatientID": "12345678",
                            "PatientFirstName": "name",
                            "PatientLastName": "family"
                        }
                }
    */

    public class ScanDocumentContext
    {
        public static void SaveDocument(ScanDocument doc)
        {
            string content = JsonConvert.SerializeObject(doc.ApplicationParams);
            using (TextWriter writer = new StreamWriter(doc.ArgsFilename, false))
            {
                writer.Write(content);
            }

            LogListItems.Instance.AppendLog("Info: Save document " + content, false);

        }
        public static void SaveDocument(ScanDocument doc, string filename)
        {
            string content = JsonConvert.SerializeObject(doc.ApplicationParams);
            using (TextWriter writer = new StreamWriter(filename, false))
            {
                writer.Write(content);
            }

            LogListItems.Instance.AppendLog("Info: Save document " + content, false);

        }
        public static ScanDocument ReadDocument(string filename)
        {
            using (TextReader reader = new StreamReader(filename))
            {
                var fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<ScanDocument>(fileContents);
            }
        }
        public static string Decode(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;
            return HttpUtility.UrlDecode(text);
        }
        public static ScanDocument ReadQuery(NameValueCollection query)
        {
            return new ScanDocument()
            {
                ScanParms = new ScanParms()
                {
                    EnableEntScan = Types.ToBool(query["EnableEntScan"], false),
                    ScanType = query["ScanType"]
                },
                ApplicationParams = new Dictionary<string, object>()
                //ApplicationParams = new ApplicationParams()
                //{
                //    DepartmentCode = query["DepartmentCode"],
                //    DepartmentName = Decode(query["DepartmentName"]),
                //    InstituteCode = query["InstituteCode"],
                //    IntituteName = Decode(query["IntituteName"]),
                //    PatientFirstName = Decode(query["PatientFirstName"]),
                //    PatientID = query["PatientID"],
                //    PatientIDType = query["PatientIDType"],
                //    PatientLastName = Decode(query["PatientLastName"]),
                //    SystemCode = query["SystemCode"],
                //    SystemName = Decode(query["SystemName"]),
                //    UserName = Decode(query["UserName"])
                //}

            };
        }
    }
    public class ScanDocument
    {
        public const string DefaultScanType = "naps2";
        public static ScanDocument Default()
        {
            return new ScanDocument()
            {
                ScanParms = new ScanParms()
                {
                    EnableEntScan = ScanSettings.enable_enterpriseScan,
                    ScanType = DefaultScanType
                },
                ApplicationParams = null
            };
        }

        public ScanDocument() {
            IsValid = false;
        }

        [JsonIgnore]
        public bool IsValid { get; private set; }
        [JsonIgnore]
        public string ScanFilename { get; set; }
        [JsonIgnore]
        public string ArgsFilename { get; set; }
        //[JsonIgnore]
        //public string RequestFilename { get; set; }
        [JsonIgnore]
        public string ScanType { get {return ScanParms==null ? DefaultScanType: string.IsNullOrEmpty(ScanParms.ScanType) ? DefaultScanType: ScanParms.ScanType; } }
        [JsonProperty]
        public ScanParms ScanParms { get; set; }
        //public ApplicationParams ApplicationParams { get; set; }
        [JsonProperty]
        public Dictionary<string,object> ApplicationParams { get; set; }
        //public object ApplicationParams { get; set; }

        //public void SetArgs()
        //{

        //    string filename = ScanSettings.document_filename;
        //    string path = ScanSettings.document_path;
        //    string docFilename = Path.GetFileNameWithoutExtension(filename);

        //    ArgsFilename = Path.Combine(path, docFilename + ".json");
        //    ScanFilename = Path.Combine(path, filename);
        //}
        public void EnsureDocument()
        {

            string filename = ScanSettings.document_filename;
            string path = ScanSettings.document_path;
            //string docFilename = Path.GetFileNameWithoutExtension(filename);

            ArgsFilename = Path.Combine(path, ScanSettings.request_filename);
            ScanFilename = Path.Combine(path, filename);

            //if (ScanParms == null)
            //    throw new ArgumentNullException("ScanParms");
            //if (ApplicationParams == null)
            //    throw new ArgumentNullException("ApplicationParams");

            if (ScanParms == null)
                ScanParms = ScanParms.Default();

            if (string.IsNullOrWhiteSpace(ScanParms.ScanType))
                ScanParms.ScanType = "naps2";

            IsValid = true;
        }

        public void SaveToFile() {

            if (ScanSettings.deleteFileBeforeNewScan)
            {
                ScanSettings.ClearDocumentFolderBeforScan();
            }
 
            //if(exitCode!=0)
            //{
            //    //LogListItems.Instance.AppendLog("Info: ExecScan naps2 has a problem.");
            //    return;
            //}
            //LogListItems.Instance.AppendLog("Info: ExecScan WinProc.RunApp.");

            if (ApplicationParams != null)
            {

                string content = JsonConvert.SerializeObject(ApplicationParams);
                using (TextWriter writer = new StreamWriter(ArgsFilename, false))
                {
                    writer.Write(content);
                }

                LogListItems.Instance.AppendLog("Info: Save document " + content, false);
            }

            //ScanDocumentContext.SaveDocument(this);
            //LogListItems.Instance.AppendLog("Info: SaveDoc Completed.", false);
        }

        public static void SaveRequest(string jsonRequest)
        {

            ScanSettings.ClearIncomingDocumentFolder();

            //if(exitCode!=0)
            //{
            //    //LogListItems.Instance.AppendLog("Info: ExecScan naps2 has a problem.");
            //    return;
            //}
            //LogListItems.Instance.AppendLog("Info: ExecScan WinProc.RunApp.");

            string filename = Path.Combine(ScanSettings.incoming_doc_path, ScanSettings.request_filename);
            
            using (TextWriter writer = new StreamWriter(filename, false))
            {
                writer.Write(jsonRequest);
            }

            LogListItems.Instance.AppendLog("Info: Save request " + jsonRequest, false);


            //ScanDocumentContext.SaveDocument(this);
            //LogListItems.Instance.AppendLog("Info: SaveDoc Completed.", false);
        }

        public static ScanDocument Create(string jsonString)
        {
            ScanDocument doc= JsonConvert.DeserializeObject<ScanDocument>(jsonString);
            if (doc == null)
            {
                throw new JsonSerializationException();
            }
            doc.EnsureDocument();
            return doc;
        }

        public static ScanDocument Deserialize(string jsonString)
        {
            return JsonConvert.DeserializeObject<ScanDocument>(jsonString);
        }

        //public static ScanDocument SaveDoc(ScanDocument doc = null)
        //{
        //    try
        //    {
        //        LogListItems.Instance.AppendLog("Info: ExecScan start.");

        //        if (doc == null)
        //            doc = ScanDocument.Default();
        //        else
        //            doc.ValidateDocument();

        //        doc.SetArgs();

        //        //LogListItems.Instance.AppendLog("Info: ExecScan ValidateDocument.");

        //        //string filename = ScanSettings.document_filename;
        //        //string path = ScanSettings.document_path;
        //        //string docFilename = Path.GetFileNameWithoutExtension(filename);
        //        //string fileDoc = Path.Combine(path, docFilename + ".json");


        //        //LogListItems.Instance.AppendLog("Info: ExecScan ScanSettings.");
        //        //LogListItems.Instance.AppendLog("Info: ExecScan scanType " + scanType);
        //        //LogListItems.Instance.AppendLog("Info: ExecScan filename " + filename);
        //        //LogListItems.Instance.AppendLog("Info: ExecScan path " + path);
        //        //LogListItems.Instance.AppendLog("Info: ExecScan docFilename " + docFilename);


        //        if (ScanSettings.deleteFileBeforeNewScan)
        //        {
        //            ScanSettings.ClearDocumentFolderBeforScan();
        //        }

        //        //if(exitCode!=0)
        //        //{
        //        //    //LogListItems.Instance.AppendLog("Info: ExecScan naps2 has a problem.");
        //        //    return;
        //        //}
        //        //LogListItems.Instance.AppendLog("Info: ExecScan WinProc.RunApp.");


        //        if (doc.ApplicationParams != null)
        //        {
        //            ScanDocumentContext.SaveDocument(doc);
        //        }

        //        LogListItems.Instance.AppendLog("Info: SaveDoc Completed.", false);

        //        return doc;
        //    }
        //    catch (Exception ex)
        //    {
        //        string err = "Error: ExecScan " + ex.Message + ", Trace: " + ex.StackTrace;
        //        LogListItems.Instance.AppendLog(err, true);
        //        Netlog.Error(err);
        //        return null;
        //    }
        //}
    }
    public class ScanParms
    {
        public static ScanParms Default()
        {

            return new ScanParms()
            {
                EnableEntScan = ScanSettings.enable_enterpriseScan,
                ScanType = "naps2"
            };
        }
        public string ScanType { get; set; }
        public bool EnableEntScan { get; set; }
    }
    public class ApplicationParams
    {
        public string SystemCode { get; set; }
        public string SystemName { get; set; }
        public string InstituteCode { get; set; }
        public string IntituteName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string UserName { get; set; }
        public string PatientIDType { get; set; }
        public string PatientID { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
    }
}
