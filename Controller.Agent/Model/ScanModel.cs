﻿using Nistec.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Agent.Model
{
    public class ScanModel
    {
       
        public ScanModel()
        {

        }

        //public void ExecScanAsync(ScanDocument doc)
        //{
        //    Task.Factory.StartNew(() => ExecScan(doc));
        //}

        //public ScanDocument SaveDoc(ScanDocument doc= null)
        //{
        //    try
        //    {
        //        LogListItems.Instance.AppendLog("Info: ExecScan start.");

        //        if (doc == null)
        //            doc = ScanDocument.Default();
        //        else
        //            doc.ValidateDocument();

        //        doc.SetArgs();

        //        //LogListItems.Instance.AppendLog("Info: ExecScan ValidateDocument.");

        //        //string filename = ScanSettings.document_filename;
        //        //string path = ScanSettings.document_path;
        //        //string docFilename = Path.GetFileNameWithoutExtension(filename);
        //        //string fileDoc = Path.Combine(path, docFilename + ".json");


        //        //LogListItems.Instance.AppendLog("Info: ExecScan ScanSettings.");
        //        //LogListItems.Instance.AppendLog("Info: ExecScan scanType " + scanType);
        //        //LogListItems.Instance.AppendLog("Info: ExecScan filename " + filename);
        //        //LogListItems.Instance.AppendLog("Info: ExecScan path " + path);
        //        //LogListItems.Instance.AppendLog("Info: ExecScan docFilename " + docFilename);


        //        if (ScanSettings.deleteFileBeforeNewScan)
        //        {
        //            ScanSettings.ClearDocumentFolderBeforScan();
        //        }

        //        //if(exitCode!=0)
        //        //{
        //        //    //LogListItems.Instance.AppendLog("Info: ExecScan naps2 has a problem.");
        //        //    return;
        //        //}
        //        //LogListItems.Instance.AppendLog("Info: ExecScan WinProc.RunApp.");


        //        if (doc.ApplicationParams != null)
        //        {
        //            ScanDocumentContext.SaveDocument(doc);
        //        }

        //        LogListItems.Instance.AppendLog("Info: SaveDoc Completed.", false);

        //        return doc;
        //    }
        //    catch (Exception ex)
        //    {
        //        string err = "Error: ExecScan " + ex.Message + ", Trace: " + ex.StackTrace;
        //        LogListItems.Instance.AppendLog(err, true);
        //        Netlog.Error(err);
        //        return null;
        //    }
        //}

        /*
        public void b_ExecScan(ScanDocument doc)
        {
            string exe = null;
            string naps_arguments = null;
            try
            {
                LogListItems.Instance.AppendLog("Info: ExecScan start.");

                if (doc == null)
                    doc = ScanDocument.Default();
                else
                    doc.ValidateDocument();

                //LogListItems.Instance.AppendLog("Info: ExecScan ValidateDocument.");

                string filename = ScanSettings.document_filename;
                string path = ScanSettings.document_path;
                string docFilename = Path.GetFileNameWithoutExtension(filename);
                string fileDoc = Path.Combine(path, docFilename+".json");
                string scanType = doc.ScanParms.ScanType;
                if (string.IsNullOrWhiteSpace(scanType))
                    scanType = "naps2";

                //LogListItems.Instance.AppendLog("Info: ExecScan ScanSettings.");
                //LogListItems.Instance.AppendLog("Info: ExecScan scanType " + scanType);
                //LogListItems.Instance.AppendLog("Info: ExecScan filename " + filename);
                //LogListItems.Instance.AppendLog("Info: ExecScan path " + path);
                //LogListItems.Instance.AppendLog("Info: ExecScan docFilename " + docFilename);


                if (scanType.ToLower() == "naps2")
                {
                    exe = ScanSettings.naps2_scan_exe_path;
                    if (exe == null)
                    {
                        throw new Exception("Invalid scanner file name");
                    }
                    if (ScanSettings.deleteFileBeforeNewScan)
                    {
                        ScanSettings.ClearDocumentFolderBeforScan();
                    }
                    naps_arguments = "-o " + Path.Combine(path,filename);
                    int waitForExit = ScanSettings.sacn_wait_seconds;
                    if (waitForExit > 0)
                        waitForExit = waitForExit * 1000;//toMiliseconds
                    bool isSilentMode = ScanSettings.sacn_in_silent_mode;
                    
                    
                    // run NAPS2
                    int exitCode = 0;
                    if (ScanSettings.IsServiceMode)
                    {
                        //Impersonation.ExecuteAppAsLoggedOnUser("naps2", null);
                        using (ProcessStarter ps = new ProcessStarter("naps2", exe, naps_arguments))
                        {
                            ps.Run(null);// Naps((uint)waitForExit);
                            exitCode = ps.WaitForExit();
                        }
                    }
                    else
                    {
                        exitCode = WinProc.RunAppSilent("ExecScan naps2", exe, naps_arguments, isSilentMode, waitForExit);
                    }

                    LogListItems.Instance.AppendLog("Info: ExecScan exitCode: " + exitCode.ToString());
                   
                    //if(exitCode!=0)
                    //{
                    //    //LogListItems.Instance.AppendLog("Info: ExecScan naps2 has a problem.");
                    //    return;
                    //}
                    //LogListItems.Instance.AppendLog("Info: ExecScan WinProc.RunApp.");
                }
                else if (scanType.ToLower() == "opentext")
                {
                    exe = ScanSettings.opentext_scan_exe_path;
                    if (exe == null)
                    {
                        throw new Exception("Invalid scanner file name");
                    }
                    throw new Exception("opentext_scan not supported");
                    //arguments = "-o " + filename;
                }

                LogListItems.Instance.AppendLog("Info: Scan document using " + scanType,false);

                if (doc.ApplicationParams != null)
                {
                    ScanDocumentContext.SaveDocument(doc, fileDoc);
                }

                LogListItems.Instance.AppendLog("Info: EnableEntScan: " + doc.ScanParms.EnableEntScan.ToString());

                if (doc.ScanParms.EnableEntScan)
                {
                    var enterprise_exe = ScanSettings.enterprise_exe_path;
                    if (string.IsNullOrEmpty(enterprise_exe))
                    {
                        LogListItems.Instance.AppendLog("Invalid Enterprise scanner directory.",true);
                        return;
                    }

                    LogListItems.Instance.AppendLog("Info: Run Enterprise scanner.", false);
                    // run enterprise scanner


                    if (ScanSettings.IsServiceMode)
                    {
                        using (ProcessStarter ps = new ProcessStarter("enterprise", enterprise_exe))
                        {
                            ps.RunEnterpriseScan();
                            //ps.WaitForExit();
                        }
                    }
                    else
                    {
                        WinProc.ShowSessionApp(enterprise_exe, null, ProcessWindowStyle.Maximized);
                    }
                   

                    //WinProc.RunApp(enterprise_exe, null, ProcessWindowStyle.Maximized, true);
                }

                LogListItems.Instance.AppendLog("Info: ExecScan Completed.", false);
            }
            catch(Exception ex)
            {
                string err = "Error: ExecScan " + ex.Message+", Trace: "+ ex.StackTrace;
                LogListItems.Instance.AppendLog(err,true);
                Netlog.Error(err);
            }

        }
        */
        public void ShowSessionApp(IntPtr ptr)
        {
            LogListItems.Instance.AppendLog("Info: ShowSessionApp.", false);
            WinProc.ActivateApp(ptr, (int)ProcessWindowStyle.Maximized);
        }

        //not used
        /*
        public void ExecScan(string filename, string scanType, bool enableEntScan)
        {
            Console.WriteLine("ExecScan " + filename);
            if (scanType == null)
                scanType = "naps2";
            scanType = scanType.ToLower();

            string exe = null;
            string naps_arguments = null;

            if (scanType == "naps2")
            {
                exe = ScanSettings.naps2_scan_exe_path;
                naps_arguments = "-o " + filename;
            }
            else if (scanType == "opentext")
            {
                exe = ConfigurationManager.AppSettings["opentext_scan_exe"];
                //arguments = "-o " + filename;
            }
            if (exe == null)
            {
                throw new Exception("Invalid scanner file name");
            }

            string path = ScanSettings.document_path;
            string fileDest = Path.Combine(path, filename);

            int waitForExit = ScanSettings.sacn_wait_seconds;
            if (waitForExit > 0)
                waitForExit = waitForExit * 1000;//toMiliseconds

            bool isSilentMode= ScanSettings.sacn_in_silent_mode;

            // run NAPS2
            WinProc.RunAppSilent(exe, naps_arguments, isSilentMode,waitForExit);

            //Process process = new Process();
            //process.StartInfo.FileName = exe;// @"C:\Program Files (x86)\NAPS2\NAPS2.Console.exe";
            //process.StartInfo.Arguments = "-o " + filename; //arguments;// "-o " + filename;
            //process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            //process.Start();
            //process.WaitForExit();// Waits here for the process to exit.


            if (enableEntScan)
            {
                var enterprise_exe = ScanSettings.enterprise_exe_path;
                if (string.IsNullOrEmpty(enterprise_exe))
                    return;
                // run enterprise scanner
                WinProc.ShowApp(enterprise_exe,null);
            }
        }
         */ 
    }
}
