﻿using Nistec.Channels;
using Nistec.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController.Model
{
    public class LogListItems
    {
        public readonly bool EnableTray = true;
        public readonly bool IsService;
        public readonly bool IsTrayServiceWatcher;
        public static readonly LogListItems Instance = new LogListItems();

        private LogListItems()
        {
            IsService = ScanSettings.IsServiceMode;
            IsTrayServiceWatcher = ScanSettings.IsTrayServiceWatcher;
            EnableTray = ScanSettings.EnableTray;

            if(EnableTray)
            {
                _LogItems = new List<string>();
            }
        }

        public const int MaxItem = 100;

        public event EventHandler<EventArgs> LogChanged;

        string _SyncLog;
        
        List<string> _LogItems;// = new List<string>();

        public List<string> LogItems
        {
            get { return _LogItems; }
        }

        public void Error(string logtext)
        {
            Netlog.Error(logtext);
            AppendLogInternal(logtext);
        }

        public void Info(string logtext)
        {
            Netlog.Info(logtext);
            AppendLogInternal(logtext);
        }

        public void AppendLog(string logtext, bool isError = false)
        {
            if (isError)
                Netlog.Error(logtext);
            else
                Netlog.Info(logtext);

            AppendLogInternal(logtext);
        }

        void AppendLogInternal(string logtext)
        {
           
            if (EnableTray)
            {
                if (_LogItems.Count > MaxItem)
                {
                    _LogItems.RemoveAt(LogItems.Count - 1);

                }
                _LogItems.Add(logtext);

                if (IsService)
                {
                    //if (EnableRemotePipe)
                    //Task.Factory.StartNew(() => PipeClientExtension.SendOut(logtext));
                }
                else
                {
                    EventHandler<EventArgs> handler = LogChanged;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }
                }
            }
        }
         public void SyncLog(string logtext)
        {
            _SyncLog = logtext;
 
            EventHandler<EventArgs> handler = LogChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public string GetLog()
        {
            string log = string.Join("\r\n", _LogItems.ToArray());
            return log;
        }
        public string GetSyncLog()
        {
            return _SyncLog;
        }
    }
}
