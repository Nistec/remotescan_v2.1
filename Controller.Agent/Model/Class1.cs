﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Agent.Remote
{

    public class WinApi
    {
        /// <summary>
        /// The WTSGetActiveConsoleSessionId function retrieves the Remote Desktop Services session that
        /// is currently attached to the physical console. The physical console is the monitor, keyboard, and mouse.
        /// Note that it is not necessary that Remote Desktop Services be running for this function to succeed.
        /// </summary>
        /// <returns>The session identifier of the session that is attached to the physical console. If there is no
        /// session attached to the physical console, (for example, if the physical console session is in the process
        /// of being attached or detached), this function returns 0xFFFFFFFF.</returns>
        [DllImport("kernel32.dll")]
        private static extern uint WTSGetActiveConsoleSessionId();


        public static uint GetSessionId()
        {
            uint result = WTSGetActiveConsoleSessionId();
            if (result == 0xFFFFFFFF)
                throw new InvalidOperationException("No session attached to the physical console.");

            return result;
        }

        [DllImport("wtsapi32.dll")]
        static extern bool WTSQueryUserToken(UInt32 sessionId, out IntPtr Token);

	 

    }

}
