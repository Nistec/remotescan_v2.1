﻿using Nistec.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace RemoteController.Model
{

    public static class ScanSettings
    {
        public static string TrayInstanceId;

        public static void Reset()
        {
            _certificate_filename=null;
            _certificate_thumbprint = null;
            _certificate_password = null;
            _document_filename = null;
            _document_path = null;
            _enable_enterprise = null;
            _deleteFileBeforeNewScan = null;
            _enable_autostart = null;
            _is_service = null;
            _naps2_scan_exe = null;
            _scan_wait_seconds = 0;
            _scan_wait = null;
            _opentext_scan_exe = null;
            _enterprise_exe = null;
            _running_mode = null;
            _is_ssl = null;
            _http_Port = null;
            _scan_in_silent_mode = null;
        }

        public const string AppName = "RemoteScan";
        public const string EXECPATH = "EXECPATH";

        public static string GetPath(string path)
        {
            if (path == null)
                return "";

            if (path.StartsWith(EXECPATH))
                return path.Replace(EXECPATH, CurrentPath);
            return path;
        }

        public static string CurrentPath
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory;// System.Reflection.Assembly.GetExecutingAssembly().Location;

            }
        }


        static string _scan_in_silent_mode;
        public static bool scan_in_silent_mode
        {
            get
            {
                if (_scan_in_silent_mode == null)
                {
                    _scan_in_silent_mode = ConfigurationManager.AppSettings["scan_in_silent_mode"];
                }
                return _scan_in_silent_mode == null ? false : _scan_in_silent_mode.ToLower() == "true";
            }
        }

        static string _is_ssl;
        public static bool is_ssl
        {
            get
            {
                if (_is_ssl == null)
                {
                    _is_ssl = ConfigurationManager.AppSettings["is_ssl"];
                }
                return _is_ssl == null ? false : _is_ssl.ToLower() == "true";
            }
        }
        static string _http_Port;
        public static string http_Port
        {
            get
            {
                if (_http_Port == null)
                {
                    _http_Port = ConfigurationManager.AppSettings["http_Port"];
                }
                return _http_Port;
            }
        }
        static string _certificate_filename;
        public static string CertificateFilename
        {
            get
            {
                if (_certificate_filename == null)
                {
                    _certificate_filename = ConfigurationManager.AppSettings["certificate_filename"];
                    if (_certificate_filename == null)
                        return null;
                    _certificate_filename = _certificate_filename.Replace(@"~\", CurrentPath);
                }
                return _certificate_filename;
            }
        }

        static string _certificate_thumbprint;
        public static string CertificateThumbprint
        {
            get
            {
                if (_certificate_thumbprint == null)
                    _certificate_thumbprint = ConfigurationManager.AppSettings["certificate_thumbprint"];
                return _certificate_thumbprint;
            }
        }

        static string _certificate_password;
        public static string CertificatePassword
        {
            get
            {
                if (_certificate_password == null)
                    _certificate_password = ConfigurationManager.AppSettings["certificate_password"];
                return _certificate_password;
            }
        }

        static string _document_filename;
        public static string document_filename
        {
            get
            {
                if (_document_filename == null)
                    _document_filename = ConfigurationManager.AppSettings["document_filename"];
                return _document_filename;
            }
        }
        static string _request_filename;
        public static string request_filename
        {
            get
            {
                if (_request_filename == null)
                    _request_filename = ConfigurationManager.AppSettings["request_filename"];
                return _request_filename;
            }
        }

        static string _document_path;
        public static string document_path
        {
            get
            {
                if (_document_path == null)
                    _document_path = GetPath(ConfigurationManager.AppSettings["document_path"]);
                return _document_path;
            }
        }

        static string _incoming_doc_path;
        public static string incoming_doc_path
        {
            get
            {
                if (_incoming_doc_path == null)
                    _incoming_doc_path = GetPath(ConfigurationManager.AppSettings["incoming_doc_path"]);
                return _incoming_doc_path;
            }
        }
        

        static string _enable_enterprise;
        public static bool enable_enterpriseScan
        {
            get
            {
                if (_enable_enterprise == null)
                    _enable_enterprise = ConfigurationManager.AppSettings["enable_enterprise"];
                return _enable_enterprise == null ? false : _enable_enterprise.ToLower() == "true";
            }
        }

        static string _deleteFileBeforeNewScan;
        public static bool deleteFileBeforeNewScan
        {
            get
            {
                if (_deleteFileBeforeNewScan == null)
                    _deleteFileBeforeNewScan = ConfigurationManager.AppSettings["deleteFileBeforeNewScan"];
                return _deleteFileBeforeNewScan == null ? false : _deleteFileBeforeNewScan.ToLower() == "true";
            }
        }
        
        static string _enable_autostart;
        public static bool enable_autostart
        {
            get
            {
                if (_enable_autostart == null)
                    _enable_autostart = ConfigurationManager.AppSettings["enable_autostart"];
                return _enable_autostart == null ? false : _enable_autostart.ToLower() == "true";
            }
        }

        static string _autostart_remote_service;
        public static bool autostart_remote_service
        {
            get
            {
                if (_autostart_remote_service == null)
                    _autostart_remote_service = ConfigurationManager.AppSettings["autostart_remote_service"];
                return _autostart_remote_service == null ? false : _autostart_remote_service.ToLower() == "true";
            }
        }

        static string _is_service;
        public static bool is_service
        {
            get
            {
                if (_is_service == null)
                    _is_service = ConfigurationManager.AppSettings["is_service"];
                return _is_service == null ? false : _is_service.ToLower() == "true";
            }
        }

        static string _naps2_scan_exe;
        public static string naps2_scan_exe_path
        {
            get
            {
                if (_naps2_scan_exe == null)
                    _naps2_scan_exe = ConfigurationManager.AppSettings["naps2_scan_exe"];
                return _naps2_scan_exe;
            }
        }

        static string _scan_wait;
        static int _scan_wait_seconds = 0;
        public static int scan_wait_seconds
        {
            get
            {
                if (_scan_wait == null)
                {
                    _scan_wait = ConfigurationManager.AppSettings["scan_wait_seconds"];

                    if (!int.TryParse(_scan_wait, out _scan_wait_seconds))
                    {
                        _scan_wait_seconds = 0;
                    }
                    _scan_wait_seconds=Math.Min(_scan_wait_seconds,180);// ensure maximum is not mor then 180 seconds

                    _scan_wait = _scan_wait_seconds.ToString();
                }
                return _scan_wait_seconds;
            }
        }

        static string _opentext_scan_exe;
        public static string opentext_scan_exe_path
        {
            get
            {
                if (_opentext_scan_exe == null)
                    _opentext_scan_exe = ConfigurationManager.AppSettings["opentext_scan_exe"];
                return _opentext_scan_exe;
            }
        }

        static string _enterprise_exe;
        public static string enterprise_exe_path
        {
            get
            {
                if (_enterprise_exe == null)
                    _enterprise_exe = ConfigurationManager.AppSettings["enterprise_exe"];
                return _enterprise_exe;
            }
        }

        //tray|service
        static string _running_mode;
        public static string running_mode
        {
            get
            {
                if (_running_mode == null)
                    _running_mode = ConfigurationManager.AppSettings["running_mode"];
                return _running_mode;
            }
        }

        //pipe|watcher
        static string _tray_service_mode;
        public static string tray_service_mode
        {
            get
            {
                if (_tray_service_mode == null)
                    _tray_service_mode = ConfigurationManager.AppSettings["tray_service_mode"];
                return _tray_service_mode;
            }
        }

        public static bool IsTrayServiceWatcher
        {
            get { return (ScanSettings.EnableTray && ScanSettings.EnableService && ScanSettings.tray_service_mode == "watcher"); }
        }

        public static bool IsServiceMode
        {
            get { return running_mode=="service"; }
        }

        public static bool EnableService
        {
            get { return running_mode.Contains("service"); }
        }
        public static bool EnableTray
        {
            get { return running_mode.Contains("tray"); }
        }
        public static void ClearDocumentFolderBeforScan()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(document_path);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }
        public static void ClearIncomingDocumentFolder()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(incoming_doc_path);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }
        public static void CreateIncomingDocumentFolder()
        {
            bool folderExists = Directory.Exists(incoming_doc_path);
            if (!folderExists)
                Directory.CreateDirectory(incoming_doc_path);
        }
        public static void CreateDocumentFolder()
        {
            bool folderExists = Directory.Exists(document_path);
            if (!folderExists)
                Directory.CreateDirectory(document_path);
        }
        public static void CreateLogFolder()
        {
            var lofFolder = Path.Combine(Directory.GetCurrentDirectory(),"Logs");
            bool folderExists = Directory.Exists(lofFolder);
            if (!folderExists)
                Directory.CreateDirectory(lofFolder);
        }
    }

}
