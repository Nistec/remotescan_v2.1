﻿using Nistec.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController.Model
{


    public class ProcessTokenaizer : IDisposable
    {

        [StructLayout(LayoutKind.Sequential)]
        public struct STARTUPINFO
        {
            public int cb;
            public String lpReserved;
            public String lpDesktop;
            public String lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public uint dwProcessId;
            public uint dwThreadId;
        }


        public enum TOKEN_TYPE
        {
            TokenPrimary = 1,
            TokenImpersonation
        }

        public enum SECURITY_IMPERSONATION_LEVEL
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        [DllImport("kernel32.dll", EntryPoint = "CloseHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public extern static bool CloseHandle(IntPtr handle);

        [DllImport("kernel32.dll")]
        public static extern uint WTSGetActiveConsoleSessionId();

        [DllImport("wtsapi32.dll", SetLastError = true)]
        public static extern bool WTSQueryUserToken(UInt32 sessionId, out IntPtr Token);

        [DllImport("advapi32.dll", EntryPoint = "CreateProcessAsUser", SetLastError = true, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public extern static bool CreateProcessAsUser(IntPtr hToken, String lpApplicationName, String lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandle, int dwCreationFlags, IntPtr lpEnvironment,
            String lpCurrentDirectory, ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);


        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool DuplicateTokenEx(
            IntPtr hExistingToken,
            uint dwDesiredAccess,
            ref SECURITY_ATTRIBUTES lpTokenAttributes,
            SECURITY_IMPERSONATION_LEVEL ImpersonationLevel,
            TOKEN_TYPE TokenType,
            out IntPtr phNewToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetLastError();
        //=======================================================

        //EventLog el;
        //EventLog ELog
        //{
        //    get
        //    {
        //        if(el==null)
        //        {
        //            el = new EventLog();
        //        }
        //        return el;
        //    }
        //}

        public int RunUserProcess(string appName, string AppPath, string arguments)
        {

            bool ret;
            SECURITY_ATTRIBUTES sa = new SECURITY_ATTRIBUTES();

            uint dwSessionID = WTSGetActiveConsoleSessionId();

            Netlog.DebugFormat("WTSGetActiveConsoleSessionId: {0} for : {1} ", dwSessionID, appName);//, EventLogEntryType.FailureAudit);


            IntPtr Token = new IntPtr();
            ret = WTSQueryUserToken((UInt32)dwSessionID, out Token);

            if (ret == false)
            {
                Netlog.WarnFormat("WTSQueryUserToken failed with: {0} for :{1} ", Marshal.GetLastWin32Error(), appName);//, EventLogEntryType.FailureAudit);

            }

            const uint MAXIMUM_ALLOWED = 0x02000000;
            IntPtr DupedToken = IntPtr.Zero;

            ret = DuplicateTokenEx(Token,
                MAXIMUM_ALLOWED,
                ref sa,
                SECURITY_IMPERSONATION_LEVEL.SecurityIdentification,
                TOKEN_TYPE.TokenPrimary,
                out DupedToken);

            if (ret == false)
            {
                Netlog.WarnFormat("DuplicateTokenEx failed with: {0}  for :{1}", Marshal.GetLastWin32Error(), appName);//, EventLogEntryType.FailureAudit);
            }
            else
            {
                Netlog.DebugFormat("DuplicateTokenEx SUCCESS  for :{0}", appName);//, EventLogEntryType.SuccessAudit);
            }

            STARTUPINFO si = new STARTUPINFO();
            si.cb = Marshal.SizeOf(si);
            //si.lpDesktop = "";

            string commandLinePath = AppPath;

            // commandLinePath example: "c:\myapp.exe c:\myconfig.xml" . cmdLineArgs can be ommited
            //commandLinePath = AppPath + " " + CmdLineArgs;

            //string command = "\"" + processPath_ + "\"";
            if ((arguments != null) && (arguments.Length != 0))
            {
                commandLinePath += " " + arguments;
            }

            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            //CreateProcessAsUser(hDuplicatedToken, NULL, lpszClientPath, NULL, NULL, FALSE,
            //                    0,
            //                    NULL, NULL, &si, &pi)
            ret = CreateProcessAsUser(DupedToken, null, commandLinePath, ref sa, ref sa, false, 0, (IntPtr)0, null, ref si, out pi);

            if (ret == false)
            {
                Netlog.WarnFormat("CreateProcessAsUser failed with: {0}  for :{1} ", Marshal.GetLastWin32Error(), appName);//, EventLogEntryType.FailureAudit);
            }
            else
            {
                Netlog.DebugFormat("CreateProcessAsUser SUCCESS.  The child PID is: {0}  for :{1}", pi.dwProcessId, appName);//, EventLogEntryType.SuccessAudit);
                CloseHandle(pi.hProcess);
                CloseHandle(pi.hThread);
            }

            ret = CloseHandle(DupedToken);
            if (ret == false)
            {
                Netlog.WarnFormat("CloseHandle LastError: {0}  for :{1} ", Marshal.GetLastWin32Error(), appName);//, EventLogEntryType.Error);
            }
            else
            {
                Netlog.DebugFormat("CloseHandle SUCCESS for :{0}", appName);//, EventLogEntryType.Information);

            }

            int errorcode = GetLastError();

            if (errorcode != 0)
                Netlog.WarnFormat("Finished with Error: {0}  for :{1} ", errorcode, appName);//, EventLogEntryType.Error);


            return errorcode;
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }


}

