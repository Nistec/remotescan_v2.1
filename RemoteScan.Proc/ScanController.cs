﻿using Controller;
using RemoteController.Model;
using Nistec.Channels;
using Nistec.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteScan.Proc
{
    public class ScanController
    {
        AgentManager sm;
        RemotePipeServer pipeserver;
        ConfigFileWatcher cfWatcher;
        public void Start()
        {
            try
            {

                Netlog.Info("Proc.ScanController start...");
                //SetStartup(1);


                //if (sm == null)// && ScanSettings.EnableTray)
                //{
                //    sm = new Controller.AgentManager();
                //    sm.Start();
                //    ScanSettings.CreateDocumentFolder();
                //}
                
                if (pipeserver == null)
                {
                    pipeserver = new RemotePipeServer();
                    pipeserver.Start();
                }

                if (cfWatcher == null)
                {
                    cfWatcher = new ConfigFileWatcher(ConfigFileWatcher.RemoteScanTrayFile);
                    cfWatcher.FileChanged += cfWatcher_FileChanged;
                    cfWatcher.Start(true);
                }

                Netlog.Info("Proc.ScanController started");
            }
            catch (Exception ex)
            {
                Netlog.Error("Error in proc Start: " + ex.Message);
            }
        }

        void cfWatcher_FileChanged(object sender, System.IO.FileSystemEventArgs e)
        {
            ScanSettings.Reset();
            if (sm != null)
                sm.Restart();
        }
    }
}
