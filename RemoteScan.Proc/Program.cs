﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RemoteScan.Proc
{
    class Program
    {
        static System.Threading.Mutex mutex = new System.Threading.Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");

        static ScanController controller;

        static Timer _timer = new Timer();

        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public static void Main()
        {
            //using (Impersonation imp = new Impersonation(BuiltinUser.NetworkService))
            //{
            //    string name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //    Console.WriteLine(name);
            //}

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                controller = new ScanController();
                controller.Start();
                //ProgramApplication app = new ProgramApplication();
                //app.InitializeComponent();
                //app.Run();
                mutex.ReleaseMutex();

                while (true)
                {
                    Console.Write("_timer_Elapsed");
                    System.Threading.Thread.Sleep(1000 * 60 * 5); // Sleep for 5 minutes
                }


                //_timer.Interval = new TimeSpan(0, 5, 0).TotalMilliseconds;
                //_timer.Elapsed += _timer_Elapsed;
                //_timer.Start();
            }
            else
            {
                // send our Win32 message to make the currently running instance
                // jump on top of all the other windows
                NativeMethods.PostMessage(
                    (IntPtr)NativeMethods.HWND_BROADCAST,
                    NativeMethods.WM_SHOWME,
                    IntPtr.Zero,
                    IntPtr.Zero);
            }

        }

        static void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //do something
            Console.WriteLine("_timer_Elapsed");
        }

        
    }
}
